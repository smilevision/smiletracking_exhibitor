/**
 * Created by Higuchi Kengo on 15/07/30.
 */

namespace ST_Exhibitor {
    'use strict';

    export class MasterConfig {
        private _domain : string;
        private _URL_SET_SERIAL : string;
        private _URL_GET_VISITOR_COUNT : string;
        private _URL_SET_CACHE : string;
        private _KEY_SERIAL_CODE : string;
        private _timeout : number;
        private _pager : IPager[];

        /**
         * コンストラクタ
         */
        public constructor () {
            console.info('Create Class MasterConfig');
        }

        get domain () : string {
            if (this._domain) {
                return this._domain
            }

            this._domain = 'https://api.smiletracking.com/api/exhibitor/';
            return this._domain;
        }

        get timeout () : number {
            if (this._timeout) {
                return this._timeout
            }

            this._timeout = 10000;

            return this._timeout;
        }


        get KEY_SERIAL_CODE () : string {
            if (this._KEY_SERIAL_CODE) {
                return this._KEY_SERIAL_CODE;
            }

            this._KEY_SERIAL_CODE = 'KEY_SERIAL_CODE_EXHIBITOR';
            return this._KEY_SERIAL_CODE;
        }

        set URL_SET_SERIAL (path : string) {
            this._URL_SET_SERIAL = path;
        }

        get URL_SET_SERIAL () : string {
            if (this._URL_SET_SERIAL) {
                return this._URL_SET_SERIAL;
            }
        }

        set URL_GET_VISITOR_COUNT (path : string) {
            this._URL_GET_VISITOR_COUNT = path;
        }

        get URL_GET_VISITOR_COUNT () : string {
            if (this._URL_GET_VISITOR_COUNT) {
                return this._URL_GET_VISITOR_COUNT;
            }
        }

        set URL_SET_CACHE (path : string) {
            this._URL_SET_CACHE = path;
        }

        get URL_SET_CACHE () : string {
            if (this._URL_SET_CACHE) {
                return this._URL_SET_CACHE;
            }
        }

        public removeURLData () : void {
            this._URL_GET_VISITOR_COUNT = '';
            this._URL_SET_CACHE = '';
            this._URL_SET_SERIAL = '';
        }

        get pager () : IPager[] {
            if (this._pager) {
                return this._pager;
            }

            this._pager =
                [{
                    text : '30件',
                    value: 30
                },
                 {
                     text : '50件',
                     value: 50
                 },
                 {
                     text : '100件',
                     value: 100
                 }]

            return this._pager;
        }
    }
}