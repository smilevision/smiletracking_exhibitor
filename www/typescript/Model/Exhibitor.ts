/**
 * Created by Higuchi Kengo on 15/07/30.
 */

namespace ST_Exhibitor {
    'use strict';

    export class Exhibitor {

        private static KEY_EXHIBITOR = 'KEY_EXHIBITOR';

        private _name : string;
        private _mail : string;
        private _consumerKey : string;

        /**
         * コンストラクタ
         */
        public constructor () {
            console.info('Create Class Exhibitor');

            this.setExhibitor(this.getLocalStorage());

        }

        public setExhibitor (data : IExhibitor) : void {
            this.setName(data.name);
            this.setMail(data.mail);
            this.setConsumerKey(data.consumer_key);

            this.setLocalStorage(data);
        }

        public setName (name : string) : void {
            this._name = name;
        }

        public setMail (mail : string) : void {
            this._mail = mail;
        }

        public setConsumerKey (key : string) : void {
            this._consumerKey = key;
        }

        public getExhibitor () : IExhibitor {
            return {
                name        : this.getName(),
                mail        : this.getMail(),
                consumer_key: this.getConsumerKey()
            }
        }

        public getName () : string {
            return this._name;
        }

        public getMail () : string {
            return this._mail;
        }

        public getConsumerKey () : string {
            return this._consumerKey;
        }

        private getLocalStorage () : IExhibitor {
            var data = localStorage.getItem(Exhibitor.KEY_EXHIBITOR);
            var exhibitor : IExhibitor;

            if (data != 'undefined' && data != null) {
                exhibitor = angular.fromJson(data);
            } else {
                exhibitor = {
                    name        : '',
                    consumer_key: ''
                }
            }

            return exhibitor;
        }

        public setLocalStorage (data : IExhibitor) : void {
            localStorage.setItem(Exhibitor.KEY_EXHIBITOR, angular.toJson(data));
        }

        public removeLocalStrage () : void {
            localStorage.removeItem(Exhibitor.KEY_EXHIBITOR)
        }
    }
}