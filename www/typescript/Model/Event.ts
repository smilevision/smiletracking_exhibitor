/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor {
    'use strict';

    export class Event {

        private static KEY_EVENT = 'KEY_EVENT'

        private _name : string;
        private _place : string;
        private _type : string;

        /**
         *
         */
        public constructor () {
            console.info('Create Class Event');
            this.setEvent(this.getLocalStorage());
        }

        /* Setter and Getter */

        public setEvent (event : IEvent) : void {
            this.setName(event.name);
            this.setPlace(event.place);
            this.setType(event.type);

            this.setLocalStorage();
        }

        public setName (name : string) : void {
            this._name = name;
        }

        public setPlace (place : string) : void {
            this._place = place;
        }

        public setType (type : string) : void {
            this._type = type;
        }

        public getEvent () : IEvent {
            return {
                name : this.getName(),
                place: this.getPlace(),
                type : this.getType()
            }
        }

        public getName () : string {
            return this._name;
        }

        public getPlace () : string {
            return this._place;
        }

        public getType () : string {
            return this._type;
        }

        private getLocalStorage () : IEvent {
            var data = localStorage.getItem(Event.KEY_EVENT);

            if (data) {
                data = angular.fromJson(data);
            } else {
                data = {
                    name : '',
                    place: '',
                    type : 'normal'
                }
            }

            return data;
        }

        public setLocalStorage () : void {
            localStorage.setItem(Event.KEY_EVENT, angular.toJson(this.getEvent()));
        }

        public removeLocalStorage () : void {
            localStorage.removeItem(Event.KEY_EVENT);
        }
    }
}