/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Tracking {
    'use strict';

    angular.module('Exhibitor.Tracking', ['ngCordova', 'ionic'])
        .config(['$httpProvider', ($httpProvider : any) => {
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';

        }]);
}