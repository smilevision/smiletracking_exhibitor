/**
 * Created by Higuchi Kengo on 15/09/03.
 */

namespace ST_Exhibitor.About {
    'use strict';

    interface Scope extends ng.IScope {
        version : string;

        goPolicy() : void;
    }

    export class AboutController {
        private className = 'AboutController #';

        public constructor (private $scope : Scope,
                            private aboutService : AboutService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class AboutController');
            this.$scope.version = this.applicationService.getVersion();

            this.$scope.goPolicy = this.goPolicy.bind(this);
        }

        private goPolicy () : void {
            console.log(this.className + 'goPolicy()');
            this.aboutService.goPolicy();
        }
    }
    angular.module('Exhibitor.Settings')
        .controller('AboutController',
        ['$scope', 'AboutService', 'ApplicationService', AboutController])
}