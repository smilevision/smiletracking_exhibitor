/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.Settings {
    'use strict';

    interface Scope extends ng.IScope {
        exhibitor : IExhibitor;
    }

    export class ExhibitorInfoController {
        private className = 'ExhibitorInfoController #';

        /**
         * コンストラクタ
         * @param $scope
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class ExhibitorInfoController');

            this.$scope.exhibitor = this.applicationService.getExhibitor().getExhibitor();
            this.$scope.exhibitor.consumer_key = '';
        }
    }

    angular.module('Exhibitor.Settings')
        .controller('ExhibitorInfoController',
        ['$scope', 'ApplicationService', ExhibitorInfoController]);
}