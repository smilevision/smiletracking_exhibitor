/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.VisitorList {
    'use strict';

    interface Scope extends ng.IScope {
        visitor : IVisitorInfo;

        setFavorite() : void;
        postNote() : void;
    }

    export class VisitorInfoController {
        private className = 'VisitorInfoController #';

        public constructor (private $scope : Scope,
                            private $stateParams : angular.ui.IStateParamsService,
                            private $ionicPopup : any,
                            private visitorInfoService : VisitorInfoService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class VisitorInfoController');

            this.$scope.visitor = null;
            this.getVisitorInfo();

            this.$scope.setFavorite = this.setFavorite.bind(this);
            this.$scope.postNote = this.postNote.bind(this);
        }

        /**
         * Visitor詳細取得
         */
        private getVisitorInfo () : void {
            console.log(this.className + 'getVisitorInfo()');

            this.visitorInfoService.getVisitorInfo(
                this.$stateParams['visitorId'],
                this.applicationService.getExhibitor(),
                this.applicationService.getMasterConfig())

                .success((response) => {
                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    } else {
                        var visitor = response.visitor;
                        this.$scope.visitor = {
                            visitor_id: visitor.visitor_id,
                            content   : visitor.content,
                            note      : visitor.note,
                            favorite  : (visitor.favorite === '1')
                        };
                    }
                })
                .error(() => {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                });
        }

        /**
         * お気に入り設定
         * @param visitor
         */
        private setFavorite () : void {
            console.log(this.className + 'setFovorite()');

            this.$scope.visitor.favorite = !this.$scope.visitor.favorite
            this.visitorInfoService.setFavorite(
                this.$scope.visitor,
                this.applicationService.getExhibitor(),
                this.applicationService.getMasterConfig())
                .success((response) => {
                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'ネットワークエラー';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    }
                })
                .error(() => {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })
        }

        /**
         * メモ送信
         */
        private postNote () : void {
            console.log(this.className + 'postNote()');

            this.visitorInfoService.postNote(
                this.$scope.visitor,
                this.applicationService.getExhibitor(),
                this.applicationService.getMasterConfig())
                .success((response) => {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'ネットワークエラー';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    }
                })
                .error(() => {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })

        }
    }

    angular.module('Exhibitor.VisitorList')
        .controller('VisitorInfoController',
        ['$scope', '$stateParams', '$ionicPopup', 'VisitorInfoService', 'ApplicationService', VisitorInfoController])
}