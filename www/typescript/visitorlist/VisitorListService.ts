/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.VisitorList {
    'use strict';

    export class VisitorListService {

        private static URL_GET_VISITOR_ALL = 'get_visitor_all.php';

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class VisitorListService');
        }

        /**
         *
         * @param exhibitor
         * @param config
         * @returns {IHttpPromise<T>}
         */
        public getVisitorList (exhibitor : Exhibitor, total : number, setting : Setting, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + VisitorListService.URL_GET_VISITOR_ALL,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    total : total,
                    count : setting.getPager().value
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }
    }

    angular.module('Exhibitor.VisitorList')
        .service('VisitorListService',
        ['$http', VisitorListService]);
}