/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.VisitorList {
    'use strict';

    export class VisitorInfoService {
        private static URL_GET_VISITOR = 'get_visitor.php';
        private static URL_SET_FAVORITE = 'set_favorite.php';
        private static URL_SET_NOTE = 'set_note.php';

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class VisitorInfoService');
        }

        /**
         *
         * @param id
         * @param exhibitor
         * @param config
         * @returns {IHttpPromise<T>}
         */
        public getVisitorInfo (id : number, exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + VisitorInfoService.URL_GET_VISITOR,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    visitor_id  : id
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }

        /**
         * お気に入り設定
         * @param visitor
         * @param exhibitor
         * @param config
         * @returns {IHttpPromise<T>}
         */
        public setFavorite (visitor : IVisitorInfo, exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + VisitorInfoService.URL_SET_FAVORITE,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    visitor_id  : visitor.visitor_id,
                    favorite    : visitor.favorite ? '1' : '0'
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }

        /**
         * メモ送信
         * @param visitor
         * @param exhibitor
         * @param config
         * @returns {IHttpPromise<T>}
         */
        public postNote (visitor : IVisitorInfo, exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + VisitorInfoService.URL_SET_NOTE,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    visitor_id  : visitor.visitor_id,
                    note        : visitor.note
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }
    }

    angular.module('Exhibitor.VisitorList')
        .service('VisitorInfoService',
        ['$http', VisitorInfoService])
}