/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Tracking {
    'use strict';

    interface Scope extends ng.IScope {

        type : boolean;
        exhibitor : Exhibitor;
        visitorNumbers : string;
        doScanner() : void;
    }

    export class TrackingController {
        private className = 'TrackingController# '

        /**
         * コンストラクタ
         * @param $scope
         * @param $ionicPopup
         * @param $cordovaBarcodeScanner
         * @param trackingService
         * @param appliactionService
         */
        public constructor (private $scope : Scope,
                            private $ionicPopup : any,
                            private $cordovaBarcodeScanner : any,
                            private trackingService : TrackingService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class TrackingController');

            this.$scope.type = (this.applicationService.getEvent().getType() === 'normal');
            this.$scope.exhibitor = this.applicationService.getExhibitor();
            this.$scope.visitorNumbers = '-';
            this.$scope.doScanner = this.doScanner.bind(this);

            this.applicationService.checkConsumerKey()
                .success((response) => {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        var exhibitor : IExhibitor = {
                            name        : '',
                            mail        : '',
                            consumer_key: ''
                        }

                        this.applicationService.getExhibitor().setExhibitor(exhibitor);
                    } else {
                        var path = response.path;

                        this.applicationService.getExhibitor().setName(response.exhibitor.name);
                        this.applicationService.getExhibitor().setMail(response.exhibitor.mail);
                        this.applicationService.getExhibitor().setConsumerKey(response.exhibitor.consumer_key);

                        this.applicationService.getMasterConfig().URL_SET_SERIAL = path.serial;
                        this.applicationService.getMasterConfig().URL_GET_VISITOR_COUNT = path.count;
                        this.applicationService.getMasterConfig().URL_SET_CACHE = path.cache;

                    }

                    this.getVisitorCount();
                })
        }

        /**
         * QRコードのスキャン
         */
        private doScanner () : void {
            console.log(this.className + 'doScanner()');
            document.addEventListener("deviceready", () => {

                this.$cordovaBarcodeScanner.scan()
                    .then((barcodeData : any) => {
                        console.log(barcodeData);
                        //console.log('path' + this.appliactionService.getMasterConfig().URL_SET_SERIAL);
                        // Success! Barcode data is here
                        if (barcodeData.text) {
                            this.trackingService.postCode(
                                barcodeData.text,
                                this.applicationService.getExhibitor(),
                                this.applicationService.getMasterConfig())
                                .success((response) => {
                                    console.log(response);
                                    response = angular.fromJson(response);

                                    if (response.error != '0') {
                                        this.trackingService.saveCode(barcodeData.text,
                                            this.applicationService.getMasterConfig());
                                        /* アラート作成 */
                                        var message = '送信できませんでした';
                                        var alertPopup = this.$ionicPopup.alert({
                                            title   : 'エラー',
                                            template: '<div style="text-align: center;">' + message + '</div>'
                                        });

                                        alertPopup.then((res : boolean) => {

                                        });
                                    } else {
                                        /* キャッシュデータがあれば送信 */
                                        var cache : string[] = [];
                                        var tmp : ICache = this.trackingService.showLocalData(this.applicationService.getMasterConfig());
                                        //console.log('LocalCache');
                                        //console.log(tmp);
                                        if (tmp) {
                                            cache = tmp.cache;
                                        }

                                        if (cache.length != 0) {
                                            this.trackingService.postCache(cache,
                                                this.applicationService.getExhibitor(),
                                                this.applicationService.getMasterConfig())
                                                .success((response) => {
                                                    console.log(response);
                                                    response = angular.fromJson(response);

                                                    if (response.error != '0') {
                                                        var reCache = response.cache;
                                                        this.trackingService.saveCodes(reCache,
                                                            this.applicationService.getMasterConfig());
                                                        var message = 'キャッシュ送信エラー';
                                                        var alertPopup = this.$ionicPopup.alert({
                                                            title   : 'エラー',
                                                            template: '<div style="text-align: center;">' + message +
                                                            '</div>'
                                                        });
                                                        alertPopup.then((res : boolean) => {

                                                        });
                                                    } else {
                                                        this.trackingService.saveCodes([],
                                                            this.applicationService.getMasterConfig());

                                                        var alertPopup = this.$ionicPopup.alert({
                                                            title   : '成功',
                                                            template: '<div style="text-align: center;">全ての来場者データは送信されました</div>'
                                                        });

                                                        alertPopup.then((res : boolean) => {

                                                        });

                                                        this.getVisitorCount();
                                                    }
                                                })
                                        }
                                    }

                                    this.getVisitorCount();

                                })
                                .error(() => {
                                    /* アラート作成 */
                                    var message = barcodeData.text + 'を保存します';
                                    var alertPopup = this.$ionicPopup.alert({
                                        title   : 'ネットワークエラー',
                                        template: '<div style="text-align: center;">' + message + '</div>'
                                    });

                                    alertPopup.then((res : boolean) => {

                                    });
                                    this.trackingService.saveCode(barcodeData.text,
                                        this.applicationService.getMasterConfig());

                                })
                        } else {
                            //cancel
                        }
                    }, (error : any) => {
                        // An error occurred
                        console.log("Error-- POST serial");
                    });
            }, false)
        }

        /**
         * 来場者一覧取得
         */
        private getVisitorCount () : void {
            console.log(this.className + 'getVisitorCount()');

            this.trackingService.getVisitorCount(this.applicationService.getExhibitor(),
                this.applicationService.getMasterConfig())
                .success((response) => {
                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    } else {

                        var numbers = response.numbers;

                        this.$scope.visitorNumbers = numbers.in_booth_all;
                    }
                })
                .error(() => {
                    /* アラート作成 */
                    var message = '来場者情報が取得できませんでした';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })
        }
    }

    angular.module('Exhibitor.Tracking')
        .controller('TrackingController',
        ['$scope', '$ionicPopup', '$cordovaBarcodeScanner', 'TrackingService', 'ApplicationService',
         TrackingController])
}