/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Login {
    'use strict';

    interface Scope extends ng.IScope {
        loginData : ILoginData;

        doAuth() : void;
    }

    export class LoginController {

        private className = 'LoginController# '

        /**
         * コンストラクタ
         * @param $scope
         * @param $state
         * @param $ionicHistory
         * @param $ionicPopup
         * @param loginService
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private $state : angular.ui.IStateService,
                            private $ionicHistory : any,
                            private $ionicPopup : any,
                            private loginService : LoginService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class LoginController');

            this.$scope.loginData = {
                id      : '',
                password: ''
            }

            this.$scope.doAuth = this.doAuth.bind(this);
        }

        /**
         * ログイン認証
         */
        private doAuth () : void {
            console.log(this.className + 'doAuth()');
            this.loginService.doAuth(this.$scope.loginData, this.applicationService.getMasterConfig())
                .success((response) => {
                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'IDまたはパスワードが違います'
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });

                    } else {
                        var exhibitor = response.exhibitor;

                        var tmp : IExhibitor = {
                            name        : exhibitor.name,
                            mail        : exhibitor.mail,
                            consumer_key: exhibitor.consumer_key
                        }

                        this.applicationService.getExhibitor().setExhibitor(tmp);

                        var path = response.path;
                        var event = response.event;

                        this.applicationService.getEvent().setEvent({
                            name : event.name,
                            place: event.place,
                            type : event.type
                        })
                        this.applicationService.getMasterConfig().URL_SET_SERIAL = path.serial;
                        this.applicationService.getMasterConfig().URL_GET_VISITOR_COUNT = path.count;

                        /* ページ遷移　*/
                        this.$ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        this.$state.transitionTo('tabs.tracking');
                    }
                })
                .error(() => {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })
        }

    }

    angular.module('Exhibitor.Login')
        .controller('LoginController',
        ['$scope', '$state', '$ionicHistory', '$ionicPopup', 'LoginService', 'ApplicationService', LoginController])
}