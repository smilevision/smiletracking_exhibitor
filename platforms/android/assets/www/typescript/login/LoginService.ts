/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Login {
    'use strict';

    export class LoginService {

        private static URL_LOGIN = 'login.php';

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class LoginService');

        }

        /**
         * ログイン認証
         * @param loginData
         * @param config
         * @returns {any}
         */
        public doAuth (loginData : ILoginData, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + LoginService.URL_LOGIN,
                data   : $.param({
                    exhibitor_id: loginData.id,
                    password    : loginData.password
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }


    }

    angular.module('Exhibitor.Login')
        .service('LoginService',
        ['$http', LoginService])
}