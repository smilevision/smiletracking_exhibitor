/**
 * Created by Higuchi Kengo on 15/07/30.
 */

namespace ST_Exhibitor.Application {
    'use strict';

    angular.module('Exhibitor.Application', ['ngCordova', 'ionic'])
        .config(['$httpProvider', ($httpProvider : any) => {
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';

        }]);
}