/**
 * Created by Higuchi Kengo on 15/07/19.
 */

namespace ST_Exhibitor.Directive {

    'use strict';

    interface Scope extends ng.IScope {
        getData : any;
        model : any;
        search : any;
        placeholder : string;

        clearSearch() : void;
    }

    /**
     * 検索フォームのDirective
     */
    export class SearchDirective implements ng.IDirective {
        public link : (scope : Scope,
                       element : ng.IAugmentedJQuery,
                       attrs : ng.IAttributes) => void;

        public restrict : string = 'E';
        public templateUrl : string = 'templates/search.html';
        public replace : boolean = true;

        /**
         * コンストラクタ
         */
        public constructor () {
            this.link = (scope : Scope, element : ng.IAugmentedJQuery, attrs : any) => {
                scope.getData = '&source';
                scope.model = '=?';
                scope.search = '=?filter';
                scope.placeholder = attrs.placeholder || ''
                attrs.minLength = attrs.minLength || 0;
                scope.search = {value: ''}

                if (attrs.class) {
                    element.addClass(attrs.class);
                }

                scope.clearSearch = () => {
                    scope.search.value = ''
                }
            }
        }
    }

    angular.module('Exhibitor.Directive')
        .directive('ionSearch', () => new SearchDirective())
}