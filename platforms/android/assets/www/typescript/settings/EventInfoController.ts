/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.Settings {
    'use strict';

    interface Scope extends ng.IScope {
        event : IEvent;
    }

    export class EventInfoController {
        private className = 'EventInfoController #';

        /**
         * コンストラクタ
         * @param $scope
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private applicationService : Application.ApplicationService) {
            this.$scope.event = this.applicationService.getEvent().getEvent();
        }
    }

    angular.module('Exhibitor.Settings')
        .controller('EventInfoController',
        ['$scope', 'ApplicationService', EventInfoController])
}