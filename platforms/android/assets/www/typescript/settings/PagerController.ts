/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.Settings {
    'use strict';

    interface Scope extends ng.IScope {
        pagerList : IPager[];
        settings : ISettings;

        doChangePager() : void;
    }

    export class PagerController {
        private className = 'PagerController# '

        /**
         * コンストラクタ
         * @param $scope
         * @param pagerService
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private pagerService : PagerService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class PagerController')

            this.$scope.settings = this.applicationService.getSettings().getSettings()
            this.getPager();

            this.$scope.doChangePager = this.doChangePager.bind(this);
        }

        /**
         *
         */
        private getPager () : void {
            console.log(this.className + 'getPager()');

            var pager = this.applicationService.getSettings().getPager();
            this.$scope.pagerList = this.applicationService.getMasterConfig().pager
            this.$scope.settings.pager = this.$scope.pagerList[this.pagerService.checkPager(pager,
                this.$scope.pagerList)];
        }

        /**
         *
         */
        private doChangePager () : void {
            console.log(this.className + 'doChangePager()')

            this.pagerService.doChangePager(this.$scope.settings.pager, this.applicationService.getSettings())
        }
    }

    angular.module('Exhibitor.Settings')
        .controller('PagerController',
        ['$scope', 'PagerService', 'ApplicationService', PagerController])
}