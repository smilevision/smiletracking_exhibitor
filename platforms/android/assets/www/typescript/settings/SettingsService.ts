/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Settings {
    'use strict';

    export class SettingsService {

        /**
         *
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class SettingsService')
        }

        /**
         * キャッシュデータの表示
         */
        public showLocalData (config : MasterConfig) : ICache {
            var data = localStorage.getItem(config.KEY_SERIAL_CODE);

            data = angular.fromJson(data);
            return data;
        }

        /**
         * ログアウト
         * @param exhibitor
         */
        public logout (exhibitor : Exhibitor, config : MasterConfig) : void {
            /* Exhibitor情報削除*/
            exhibitor.removeLocalStrage();
            /* URL情報削除 */
            config.removeURLData();
            /* キャッシュデータの削除 */
            localStorage.removeItem(config.KEY_SERIAL_CODE);
        }

        /**
         * コード送信
         * @param code
         * @param exhibitor
         * @param config
         * @returns {any}
         */
        public postCode (code : string, exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + config.URL_SET_SERIAL,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    serial_code : code
                }),
                timeout: config.timeout
            }

            return this.$http(req)
        }

        /**
         * キャッシュの送信
         * @param cache
         * @param exhibitor
         * @param config
         * @returns {any}
         */
        public postCache (cache : string[], exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + config.URL_SET_CACHE,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    cache       : cache
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }

        /**
         * 送信できなかった場合にシリアルコードをキャッシュ
         * @param code
         * @param config
         */
        public saveCode (code : string, config : MasterConfig) : void {
            var data = localStorage.getItem(config.KEY_SERIAL_CODE)

            if (data) {
                data = angular.fromJson(data);
                var tmp : string[] = data.cache;
                tmp.push(code);
                var cache : ICache = {
                    cache: tmp
                }
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
            } else {
                var tmp : string[] = [code];
                var cache : ICache = {
                    cache: tmp
                }
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache))
            }
        }

        /**
         * シリアルコードを複数キャッシュ
         * @param codes
         * @param config
         */
        public saveCodes (codes : string[], config : MasterConfig) : void {
            var cache : ICache = {
                cache: codes
            }
            localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
        }
    }

    angular.module('Exhibitor.Settings')
        .service('SettingsService',
        ['$http', SettingsService])
}