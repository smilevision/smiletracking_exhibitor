/**
 * Created by Higuchi Kengo on 15/08/26.
 */
namespace ST_Exhibitor.Settings {
    'use strict';

    export class PagerService {

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class PagerService')
        }

        /**
         *
         * @param pager
         * @param settings
         */
        public doChangePager (pager : IPager, settings : Setting) : void {
            settings.setPager(pager)
            settings.setLocalStorage();
        }

        /**
         *
         * @param pager
         * @param pagerArray
         * @returns {number}
         */
        public checkPager (pager : IPager, pagerArray : IPager[]) : number {
            var index = -1;
            pagerArray.forEach((p, i) => {
                if (p.value === pager.value) {
                    index = i
                }
            })
            return index;
        }
    }

    angular.module('Exhibitor.Settings')
        .service('PagerService',
        ['$http', PagerService])
}