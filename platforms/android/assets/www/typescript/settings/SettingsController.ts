/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Settings {
    'use strict';

    interface Scope extends ng.IScope {
        showLocalData() : void;
        numberCache : number;
        logout() : void;
    }

    export class SettingsController {

        private className = 'SettingsController# '

        /**
         * コンストラクタ
         * @param $scope
         * @param $ionicPopup
         * @param settingsService
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private $state : angular.ui.IStateService,
                            private $ionicHistory : any,
                            private $ionicPopup : any,
                            private settingsService : SettingsService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class SettingsController');

            var cache : string[] = [];
            var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
            //console.log('LocalCache');
            //console.log(tmp);
            if (tmp) {
                cache = tmp.cache;
            }
            this.$scope.numberCache = cache.length;
            this.$scope.showLocalData = this.showLocalData.bind(this);
            this.$scope.logout = this.logout.bind(this);

        }

        /**
         *
         */
        private showLocalData () : void {
            console.log(this.className + 'showLocalData()');

            var cache : string[] = [];
            var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
            console.log('LocalCache');
            console.log(tmp);
            if (tmp) {
                cache = tmp.cache;
            }

            if (cache.length != 0) {
                var confirmPopup = this.$ionicPopup.confirm({
                    title   : 'キャッシュデータ送信',
                    template: '<div style="text-align: center;">キャッシュデータが存在します送信しますか？</div>'
                });

                confirmPopup.then((res : boolean) => {
                    if (res) {
                        this.settingsService.postCache(cache, this.applicationService.getExhibitor(),
                            this.applicationService.getMasterConfig())
                            .success((response) => {

                                console.log(response);
                                response = angular.fromJson(response);

                                if (response.error != '0') {
                                    var reCache = response.cache;
                                    this.settingsService.saveCodes(reCache,
                                        this.applicationService.getMasterConfig());
                                    var message = 'ネットワークエラー';
                                    var alertPopup = this.$ionicPopup.alert({
                                        title   : 'エラー',
                                        template: '<div style="text-align: center;">' + message + '</div>'
                                    });
                                    alertPopup.then((res : boolean) => {

                                    });
                                } else {
                                    this.settingsService.saveCodes([],
                                        this.applicationService.getMasterConfig());
                                    this.$scope.numberCache = 0;
                                    var alertPopup = this.$ionicPopup.alert({
                                        title   : '成功',
                                        template: '<div style="text-align: center;">全ての来場者データは送信されました</div>'
                                    });

                                    alertPopup.then((res : boolean) => {

                                    });
                                }
                            })
                            .error(() => {
                                var message = '再度お試しください';
                                var alertPopup = this.$ionicPopup.alert({
                                    title   : 'ネットワークエラー',
                                    template: '<div style="text-align: center;">' + message + '</div>'
                                });
                                alertPopup.then((res : boolean) => {

                                });
                            })

                    } else {
                        /* アラート作成 */
                        var alertPopup = this.$ionicPopup.alert({
                            title   : '注意',
                            template: '<div style="text-align: center;">ログアウトする前に必ず送信するようにしてください</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    }
                });
            } else {
                var alertPopup = this.$ionicPopup.alert({
                    title   : 'OK',
                    template: '<div style="text-align: center;">全ての来場者データは送信されています</div>'
                });

                alertPopup.then((res : boolean) => {

                });
            }
        }

        /**
         * ログアウト
         */
        private logout () : void {
            console.log(this.className + 'logout()');
            var cache : string[] = [];
            var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
            //console.log('LocalCache');
            //console.log(tmp);
            if (tmp) {
                cache = tmp.cache;
            }

            if (cache.length != 0) {
                this.settingsService.postCache(cache, this.applicationService.getExhibitor(),
                    this.applicationService.getMasterConfig())
                    .success((response) => {
                        response = angular.fromJson(response);
                        if (response.error != '0') {

                            var confirmPopup = this.$ionicPopup.show({
                                title   : 'エラー',
                                subtitle: 'ネットワークエラー',
                                template: '<div style="text-align: center;">このままログアウトすると来場者情報が削除されてしまいますがよろしいですか？</div>',
                                scope   : this.$scope,
                                buttons : [
                                    {
                                        text : 'ログアウトする',
                                        type : 'button-assertive',
                                        onTap: (e : any) => {
                                            this.settingsService.logout(this.applicationService.getExhibitor(),
                                                this.applicationService.getMasterConfig());
                                            /* ページ遷移 */
                                            this.$ionicHistory.nextViewOptions({
                                                disableBack: true
                                            });
                                            this.$state.transitionTo('login');
                                        }
                                    },
                                    {
                                        text : 'キャンセル',
                                        type : 'button-positive',
                                        onTap: (e : any) => {
                                            var alertPopup = this.$ionicPopup.alert({
                                                title   : 'ログアウトしませんでした',
                                                template: '<div style="text-align: center;">ネットワークが繋がる場所で再度お試しください</div>'
                                            });

                                            alertPopup.then((res : boolean) => {

                                            });
                                        }
                                    }
                                ]
                            })
                        } else {
                            //キャッシュデータ送信成功
                            this.settingsService.logout(this.applicationService.getExhibitor(),
                                this.applicationService.getMasterConfig());
                            /* ページ遷移 */
                            this.$ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            this.$state.transitionTo('login');
                        }
                    })
                    .error(() => {
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'ログアウトしませんでした',
                            template: '<div style="text-align: center;">ネットワークが繋がる場所で再度お試しください</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    })
            } else {
                //キャッシュデータなし
                this.settingsService.logout(this.applicationService.getExhibitor(),
                    this.applicationService.getMasterConfig());
                /* ページ遷移 */
                this.$ionicHistory.nextViewOptions({
                    disableBack: true
                });
                this.$state.transitionTo('login');
            }

        }

    }

    angular.module('Exhibitor.Settings')
        .controller('SettingsController',
        ['$scope', '$state', '$ionicHistory', '$ionicPopup', 'SettingsService', 'ApplicationService',
         SettingsController])
}