/**
 * Created by Higuchi Kengo on 15/07/30.
 */
///<reference path="../typings/jquery/jquery.d.ts"/>
///<reference path="../typings/angularjs/angular.d.ts"/>
///<reference path="../typings/angular-ui-router/angular-ui-router.d.ts"/>
///<reference path="../typings/cordova/cordova.d.ts"/>
///<reference path="../typings/cordova-ionic/cordova-ionic.d.ts"/>
///<reference path="../typings/smile/exhibitor.d.ts"/>

///<reference path="./Model/Exhibitor.ts"/>
///<reference path="./Model/MasterConfig.ts"/>
///<reference path="./Model/Event.ts"/>
///<reference path="./Model/Setting.ts"/>
///<reference path="./Directive/index.ts"/>

///<reference path="./application/index.ts"/>
///<reference path="./login/index.ts"/>
///<reference path="./tracking/index.ts"/>
///<reference path="./visitorlist/index.ts"/>
///<reference path="./settings/index.ts"/>

namespace ST_Exhibitor {
    'use strict';

    angular.module('starter',
        ['ionic',
         'ngCordova',
         'ngTouch',
         'ui.router',
         'Exhibitor.Application',
         'Exhibitor.Login',
         'Exhibitor.Directive',
         'Exhibitor.Tracking',
         'Exhibitor.VisitorList',
         'Exhibitor.Settings'])

        .run(function ($ionicPlatform : any) {
            $ionicPlatform.ready(function () {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    window.StatusBar.styleLightContent();
                }
            });
        })

        /* ログイン状態かどうかの確認 */
        .run(['$rootScope', '$state', '$ionicHistory', 'ApplicationService',
              function ($rootScope : ng.IRootScopeService,
                        $state : angular.ui.IStateService,
                        $ionicHistory : any,
                        applicationService : Application.ApplicationService) {

                  $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

                      if (toState.isLoginRequired) {
                          if (!applicationService.isLoggedIn()) {
                              console.warn('Not Login')
                              e.preventDefault();
                              $ionicHistory.nextViewOptions({
                                  disableBack: true
                              });
                              $state.go('login');
                          }

                      }
                  });
              }])

        .config(['$stateProvider', '$urlRouterProvider',
                 function ($stateProvider : angular.ui.IStateProvider, $urlRouterProvider : angular.ui.IUrlRouterProvider) {

                     // Ionic uses AngularUI Router which uses the concept of states
                     // Learn more here: https://github.com/angular-ui/ui-router
                     // Set up the various states which the app can be in.
                     // Each state's controller can be found in controllers.js
                     $stateProvider

                         .state('login', {
                             url        : '/login',
                             templateUrl: 'templates/login.html',
                             controller : 'LoginController'

                         })

                         // setup an abstract state for the tabs directive
                         .state('tabs', {
                             url            : '/app',
                             abstract       : true,
                             templateUrl    : 'templates/tabs.html',
                             controller     : 'ApplicationController',
                             isLoginRequired: true
                         })

                         // Each tab has its own nav history stack:

                         .state('tabs.tracking', {
                             cache          : false,
                             url            : '/tracking',
                             views          : {
                                 'tracking': {
                                     templateUrl: 'templates/tracking.html',
                                     controller : 'TrackingController'
                                 }
                             },
                             isLoginRequired: true
                         })

                         .state('tabs.visitorlist', {
                             cache          : false,
                             url            : '/visitorlist',
                             views          : {
                                 'tracking': {
                                     templateUrl: 'templates/visitorlist.html',
                                     controller : 'VisitorListController'
                                 }
                             },
                             isLoginRequired: true
                         })

                         .state('tabs.visitorinfo', {
                             cache          : false,
                             url            : '/visitorinfo/:visitorId',
                             views          : {
                                 'tracking': {
                                     templateUrl: 'templates/visitorinfo.html',
                                     controller : 'VisitorInfoController'
                                 }
                             },
                             isLoginRequired: true
                         })

                         .state('tabs.settings', {
                             cache          : false,
                             url            : '/settings',
                             views          : {
                                 'settings': {
                                     templateUrl: 'templates/settings.html',
                                     controller : 'SettingsController'
                                 }
                             },
                             isLoginRequired: true
                         })

                         .state('tabs.pager', {
                             cache: false,
                             url  : '/pager',
                             views: {
                                 'settings': {
                                     templateUrl: 'templates/change_pager.html',
                                     controller : 'PagerController'
                                 }
                             }
                         })
                         .state('tabs.eventinfo', {
                             url  : '/eventinfo',
                             views: {
                                 'settings': {
                                     templateUrl: 'templates/eventinfo.html',
                                     controller : 'EventInfoController'
                                 }
                             }
                         })
                         .state('tabs.exhibitorinfo', {
                             url  : '/exhibitorinfo',
                             views: {
                                 'settings': {
                                     templateUrl: 'templates/exhibitorinfo.html',
                                     controller : 'ExhibitorInfoController'
                                 }
                             }
                         })

                         .state('tabs.about', {
                             url  : '/about',
                             views: {
                                 'settings': {
                                     templateUrl: 'templates/about.html',
                                     controller : 'AboutController'
                                 }
                             }
                         });

                     // if none of the above states are matched, use this as the fallback
                     // to fix inifinite Loop
                     $urlRouterProvider.otherwise(function ($injector : any) {
                         var $state = $injector.get('$state');
                         $state.go('tabs.tracking');
                     });
                 }])

        .config(function ($ionicConfigProvider : any) {
            $ionicConfigProvider.views.swipeBackEnabled(false);
        });

}
