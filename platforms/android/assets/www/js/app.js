/**
 * Created by Higuchi Kengo on 15/07/30.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    'use strict';
    var Exhibitor = (function () {
        /**
         * コンストラクタ
         */
        function Exhibitor() {
            console.info('Create Class Exhibitor');
            this.setExhibitor(this.getLocalStorage());
        }
        Exhibitor.prototype.setExhibitor = function (data) {
            this.setName(data.name);
            this.setMail(data.mail);
            this.setConsumerKey(data.consumer_key);
            this.setLocalStorage(data);
        };
        Exhibitor.prototype.setName = function (name) {
            this._name = name;
        };
        Exhibitor.prototype.setMail = function (mail) {
            this._mail = mail;
        };
        Exhibitor.prototype.setConsumerKey = function (key) {
            this._consumerKey = key;
        };
        Exhibitor.prototype.getExhibitor = function () {
            return {
                name: this.getName(),
                mail: this.getMail(),
                consumer_key: this.getConsumerKey()
            };
        };
        Exhibitor.prototype.getName = function () {
            return this._name;
        };
        Exhibitor.prototype.getMail = function () {
            return this._mail;
        };
        Exhibitor.prototype.getConsumerKey = function () {
            return this._consumerKey;
        };
        Exhibitor.prototype.getLocalStorage = function () {
            var data = localStorage.getItem(Exhibitor.KEY_EXHIBITOR);
            var exhibitor;
            if (data != 'undefined' && data != null) {
                exhibitor = angular.fromJson(data);
            }
            else {
                exhibitor = {
                    name: '',
                    consumer_key: ''
                };
            }
            return exhibitor;
        };
        Exhibitor.prototype.setLocalStorage = function (data) {
            localStorage.setItem(Exhibitor.KEY_EXHIBITOR, angular.toJson(data));
        };
        Exhibitor.prototype.removeLocalStrage = function () {
            localStorage.removeItem(Exhibitor.KEY_EXHIBITOR);
        };
        Exhibitor.KEY_EXHIBITOR = 'KEY_EXHIBITOR';
        return Exhibitor;
    })();
    ST_Exhibitor.Exhibitor = Exhibitor;
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    'use strict';
    var MasterConfig = (function () {
        /**
         * コンストラクタ
         */
        function MasterConfig() {
            console.info('Create Class MasterConfig');
        }
        Object.defineProperty(MasterConfig.prototype, "domain", {
            get: function () {
                if (this._domain) {
                    return this._domain;
                }
                this._domain = 'https://api.smiletracking.com/api/exhibitor/';
                return this._domain;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MasterConfig.prototype, "timeout", {
            get: function () {
                if (this._timeout) {
                    return this._timeout;
                }
                this._timeout = 10000;
                return this._timeout;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MasterConfig.prototype, "KEY_SERIAL_CODE", {
            get: function () {
                if (this._KEY_SERIAL_CODE) {
                    return this._KEY_SERIAL_CODE;
                }
                this._KEY_SERIAL_CODE = 'KEY_SERIAL_CODE_EXHIBITOR';
                return this._KEY_SERIAL_CODE;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MasterConfig.prototype, "URL_SET_SERIAL", {
            get: function () {
                if (this._URL_SET_SERIAL) {
                    return this._URL_SET_SERIAL;
                }
            },
            set: function (path) {
                this._URL_SET_SERIAL = path;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MasterConfig.prototype, "URL_GET_VISITOR_COUNT", {
            get: function () {
                if (this._URL_GET_VISITOR_COUNT) {
                    return this._URL_GET_VISITOR_COUNT;
                }
            },
            set: function (path) {
                this._URL_GET_VISITOR_COUNT = path;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MasterConfig.prototype, "URL_SET_CACHE", {
            get: function () {
                if (this._URL_SET_CACHE) {
                    return this._URL_SET_CACHE;
                }
            },
            set: function (path) {
                this._URL_SET_CACHE = path;
            },
            enumerable: true,
            configurable: true
        });
        MasterConfig.prototype.removeURLData = function () {
            this._URL_GET_VISITOR_COUNT = '';
            this._URL_SET_CACHE = '';
            this._URL_SET_SERIAL = '';
        };
        Object.defineProperty(MasterConfig.prototype, "pager", {
            get: function () {
                if (this._pager) {
                    return this._pager;
                }
                this._pager =
                    [{
                            text: '30件',
                            value: 30
                        },
                        {
                            text: '50件',
                            value: 50
                        },
                        {
                            text: '100件',
                            value: 100
                        }];
                return this._pager;
            },
            enumerable: true,
            configurable: true
        });
        return MasterConfig;
    })();
    ST_Exhibitor.MasterConfig = MasterConfig;
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    'use strict';
    var Event = (function () {
        /**
         *
         */
        function Event() {
            console.info('Create Class Event');
            this.setEvent(this.getLocalStorage());
        }
        /* Setter and Getter */
        Event.prototype.setEvent = function (event) {
            this.setName(event.name);
            this.setPlace(event.place);
            this.setType(event.type);
            this.setLocalStorage();
        };
        Event.prototype.setName = function (name) {
            this._name = name;
        };
        Event.prototype.setPlace = function (place) {
            this._place = place;
        };
        Event.prototype.setType = function (type) {
            this._type = type;
        };
        Event.prototype.getEvent = function () {
            return {
                name: this.getName(),
                place: this.getPlace(),
                type: this.getType()
            };
        };
        Event.prototype.getName = function () {
            return this._name;
        };
        Event.prototype.getPlace = function () {
            return this._place;
        };
        Event.prototype.getType = function () {
            return this._type;
        };
        Event.prototype.getLocalStorage = function () {
            var data = localStorage.getItem(Event.KEY_EVENT);
            if (data) {
                data = angular.fromJson(data);
            }
            else {
                data = {
                    name: '',
                    place: '',
                    type: 'normal'
                };
            }
            return data;
        };
        Event.prototype.setLocalStorage = function () {
            localStorage.setItem(Event.KEY_EVENT, angular.toJson(this.getEvent()));
        };
        Event.prototype.removeLocalStorage = function () {
            localStorage.removeItem(Event.KEY_EVENT);
        };
        Event.KEY_EVENT = 'KEY_EVENT';
        return Event;
    })();
    ST_Exhibitor.Event = Event;
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    'use strict';
    var Setting = (function () {
        /**
         * コンストラクタ
         */
        function Setting() {
            console.info('Create Class Setting');
            this.setSettings(this.getLocalStorage());
        }
        /* Setter and Getter */
        Setting.prototype.setSettings = function (settings) {
            this.setPager(settings.pager);
            this.setLocalStorage();
        };
        Setting.prototype.setPager = function (pager) {
            this.pager = pager;
        };
        Setting.prototype.getSettings = function () {
            return {
                pager: this.getPager()
            };
        };
        Setting.prototype.getPager = function () {
            return this.pager;
        };
        /**
         *
         * @returns {*}
         */
        Setting.prototype.getLocalStorage = function () {
            var data = localStorage.getItem(Setting.KEY_SETTINGS);
            if (data) {
                data = angular.fromJson(data);
            }
            else {
                /* 設定情報が保存されていない時 */
                var master = new ST_Exhibitor.MasterConfig();
                data = {
                    pager: master.pager[0]
                };
            }
            return data;
        };
        /**
         *
         */
        Setting.prototype.setLocalStorage = function () {
            localStorage.setItem(Setting.KEY_SETTINGS, angular.toJson(this.getSettings()));
        };
        /**
         *
         */
        Setting.prototype.removeStorage = function () {
            localStorage.removeItem(Setting.KEY_SETTINGS);
        };
        Setting.KEY_SETTINGS = 'KEY_SETTINGS';
        return Setting;
    })();
    ST_Exhibitor.Setting = Setting;
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/19.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Directive;
    (function (Directive) {
        'use strict';
        angular.module('Exhibitor.Directive', ['ngCordova', 'ionic']);
    })(Directive = ST_Exhibitor.Directive || (ST_Exhibitor.Directive = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/19.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Directive;
    (function (Directive) {
        'use strict';
        /**
         * 検索フォームのDirective
         */
        var SearchDirective = (function () {
            /**
             * コンストラクタ
             */
            function SearchDirective() {
                this.restrict = 'E';
                this.templateUrl = 'templates/search.html';
                this.replace = true;
                this.link = function (scope, element, attrs) {
                    scope.getData = '&source';
                    scope.model = '=?';
                    scope.search = '=?filter';
                    scope.placeholder = attrs.placeholder || '';
                    attrs.minLength = attrs.minLength || 0;
                    scope.search = { value: '' };
                    if (attrs.class) {
                        element.addClass(attrs.class);
                    }
                    scope.clearSearch = function () {
                        scope.search.value = '';
                    };
                };
            }
            return SearchDirective;
        })();
        Directive.SearchDirective = SearchDirective;
        angular.module('Exhibitor.Directive')
            .directive('ionSearch', function () { return new SearchDirective(); });
    })(Directive = ST_Exhibitor.Directive || (ST_Exhibitor.Directive = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 2016/01/05.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Directive;
    (function (Directive) {
        'use strict';
        var TouchStartDirectives = (function () {
            //public restrict : string = 'AE';
            /**
             * コンストラクタ
             */
            function TouchStartDirectives($parse, $swipe) {
                var _this = this;
                this.$parse = $parse;
                this.$swipe = $swipe;
                this.link = function (scope, element, attrs) {
                    var handler = _this.$parse(attrs.myTouchstart);
                    _this.$swipe.bind(element, {
                        start: function () {
                            scope.$apply(function () {
                                handler(scope);
                            });
                        }
                    });
                };
            }
            return TouchStartDirectives;
        })();
        Directive.TouchStartDirectives = TouchStartDirectives;
        angular.module('Exhibitor.Directive')
            .directive('myTouchstart', ['$parse', '$swipe', function ($parse, $swipe) {
                return new TouchStartDirectives($parse, $swipe);
            }]);
    })(Directive = ST_Exhibitor.Directive || (ST_Exhibitor.Directive = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/19.
 */
///<reference path="./module.ts"/>
///<reference path="./SearchDirective.ts"/>
///<reference path="./TouchStartDirectives.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Application;
    (function (Application) {
        'use strict';
        angular.module('Exhibitor.Application', ['ngCordova', 'ionic'])
            .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
            }]);
    })(Application = ST_Exhibitor.Application || (ST_Exhibitor.Application = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Application;
    (function (Application) {
        'use strict';
        var ApplicationController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param applicationService
             */
            function ApplicationController($scope, applicationService) {
                this.$scope = $scope;
                this.applicationService = applicationService;
                this.className = 'ApplicationController# ';
                console.info('Create Class ApplicationController');
            }
            return ApplicationController;
        })();
        Application.ApplicationController = ApplicationController;
        angular.module('Exhibitor.Application')
            .controller('ApplicationController', ['$scope', 'ApplicationService', ApplicationController]);
    })(Application = ST_Exhibitor.Application || (ST_Exhibitor.Application = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Application;
    (function (Application) {
        'use strict';
        var ApplicationService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function ApplicationService($http) {
                var _this = this;
                this.$http = $http;
                console.info('Create Class ApplicationService');
                this._exhibitor = new ST_Exhibitor.Exhibitor();
                this._event = new ST_Exhibitor.Event();
                this._masterConfig = new ST_Exhibitor.MasterConfig();
                this._settings = new ST_Exhibitor.Setting();
                this._version = '1.1.0';
                if (this._exhibitor.getConsumerKey() != '') {
                    this.checkConsumerKey()
                        .success(function (response) {
                        response = angular.fromJson(response);
                        if (response.error != '0') {
                            var exhibitor = {
                                name: '',
                                mail: '',
                                consumer_key: ''
                            };
                            _this._exhibitor.setExhibitor(exhibitor);
                        }
                        else {
                            var path = response.path;
                            var event = response.event;
                            _this._exhibitor.setName(response.exhibitor.name);
                            _this._exhibitor.setMail(response.exhibitor.mail);
                            _this._exhibitor.setConsumerKey(response.exhibitor.consumer_key);
                            _this._event.setEvent({
                                name: event.name,
                                place: event.place,
                                type: event.type
                            });
                            _this._masterConfig.URL_SET_SERIAL = path.serial;
                            _this._masterConfig.URL_GET_VISITOR_COUNT = path.count;
                            _this._masterConfig.URL_SET_CACHE = path.cache;
                        }
                    })
                        .error(function () {
                        var exhibitor = {
                            name: '',
                            mail: '',
                            consumer_key: ''
                        };
                        _this._exhibitor.setExhibitor(exhibitor);
                    });
                }
            }
            /* Getter */
            ApplicationService.prototype.getExhibitor = function () {
                return this._exhibitor;
            };
            ApplicationService.prototype.getEvent = function () {
                return this._event;
            };
            ApplicationService.prototype.getMasterConfig = function () {
                return this._masterConfig;
            };
            ApplicationService.prototype.getSettings = function () {
                return this._settings;
            };
            ApplicationService.prototype.getVersion = function () {
                return this._version;
            };
            /**
             * ログインしているかどうか
             * @returns {boolean}
             */
            ApplicationService.prototype.isLoggedIn = function () {
                return (this._exhibitor.getConsumerKey() != '');
            };
            /**
             * コンシューマーキーのチェック
             * @returns {any}
             */
            ApplicationService.prototype.checkConsumerKey = function () {
                var req = {
                    method: 'POST',
                    url: this._masterConfig.domain + 'validate_auth.php',
                    data: $.param({
                        consumer_key: this._exhibitor.getConsumerKey()
                    }),
                    timeout: this._masterConfig.timeout
                };
                return this.$http(req);
            };
            /**
             * アプリケーションの初期化
             */
            ApplicationService.prototype.removeData = function () {
                this._exhibitor.removeLocalStrage();
                this._exhibitor.setExhibitor({
                    name: '',
                    consumer_key: ''
                });
            };
            return ApplicationService;
        })();
        Application.ApplicationService = ApplicationService;
        angular.module('Exhibitor.Application')
            .service('ApplicationService', ['$http', ApplicationService]);
    })(Application = ST_Exhibitor.Application || (ST_Exhibitor.Application = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
///<reference path="./module.ts"/>
///<reference path="./ApplicationController.ts"/>
///<reference path="./ApplicationService.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Login;
    (function (Login) {
        'use strict';
        angular.module('Exhibitor.Login', ['ngCordova', 'ionic'])
            .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
            }]);
    })(Login = ST_Exhibitor.Login || (ST_Exhibitor.Login = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Login;
    (function (Login) {
        'use strict';
        var LoginController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param $state
             * @param $ionicHistory
             * @param $ionicPopup
             * @param loginService
             * @param applicationService
             */
            function LoginController($scope, $state, $ionicHistory, $ionicPopup, loginService, applicationService) {
                this.$scope = $scope;
                this.$state = $state;
                this.$ionicHistory = $ionicHistory;
                this.$ionicPopup = $ionicPopup;
                this.loginService = loginService;
                this.applicationService = applicationService;
                this.className = 'LoginController# ';
                console.info('Create Class LoginController');
                this.$scope.loginData = {
                    id: '',
                    password: ''
                };
                this.$scope.doAuth = this.doAuth.bind(this);
            }
            /**
             * ログイン認証
             */
            LoginController.prototype.doAuth = function () {
                var _this = this;
                console.log(this.className + 'doAuth()');
                this.loginService.doAuth(this.$scope.loginData, this.applicationService.getMasterConfig())
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'IDまたはパスワードが違います';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                    else {
                        var exhibitor = response.exhibitor;
                        var tmp = {
                            name: exhibitor.name,
                            mail: exhibitor.mail,
                            consumer_key: exhibitor.consumer_key
                        };
                        _this.applicationService.getExhibitor().setExhibitor(tmp);
                        var path = response.path;
                        var event = response.event;
                        _this.applicationService.getEvent().setEvent({
                            name: event.name,
                            place: event.place,
                            type: event.type
                        });
                        _this.applicationService.getMasterConfig().URL_SET_SERIAL = path.serial;
                        _this.applicationService.getMasterConfig().URL_GET_VISITOR_COUNT = path.count;
                        /* ページ遷移　*/
                        _this.$ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        _this.$state.transitionTo('tabs.tracking');
                    }
                })
                    .error(function () {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                });
            };
            return LoginController;
        })();
        Login.LoginController = LoginController;
        angular.module('Exhibitor.Login')
            .controller('LoginController', ['$scope', '$state', '$ionicHistory', '$ionicPopup', 'LoginService', 'ApplicationService', LoginController]);
    })(Login = ST_Exhibitor.Login || (ST_Exhibitor.Login = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Login;
    (function (Login) {
        'use strict';
        var LoginService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function LoginService($http) {
                this.$http = $http;
                console.info('Create Class LoginService');
            }
            /**
             * ログイン認証
             * @param loginData
             * @param config
             * @returns {any}
             */
            LoginService.prototype.doAuth = function (loginData, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + LoginService.URL_LOGIN,
                    data: $.param({
                        exhibitor_id: loginData.id,
                        password: loginData.password
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            LoginService.URL_LOGIN = 'login.php';
            return LoginService;
        })();
        Login.LoginService = LoginService;
        angular.module('Exhibitor.Login')
            .service('LoginService', ['$http', LoginService]);
    })(Login = ST_Exhibitor.Login || (ST_Exhibitor.Login = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
///<reference path="./module.ts"/>
///<reference path="./LoginController.ts"/>
///<reference path="./LoginService.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Tracking;
    (function (Tracking) {
        'use strict';
        angular.module('Exhibitor.Tracking', ['ngCordova', 'ionic'])
            .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
            }]);
    })(Tracking = ST_Exhibitor.Tracking || (ST_Exhibitor.Tracking = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Tracking;
    (function (Tracking) {
        'use strict';
        var TrackingController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param $ionicPopup
             * @param $cordovaBarcodeScanner
             * @param trackingService
             * @param appliactionService
             */
            function TrackingController($scope, $ionicPopup, $cordovaBarcodeScanner, trackingService, applicationService) {
                var _this = this;
                this.$scope = $scope;
                this.$ionicPopup = $ionicPopup;
                this.$cordovaBarcodeScanner = $cordovaBarcodeScanner;
                this.trackingService = trackingService;
                this.applicationService = applicationService;
                this.className = 'TrackingController# ';
                console.info('Create Class TrackingController');
                this.$scope.type = (this.applicationService.getEvent().getType() === 'normal');
                this.$scope.exhibitor = this.applicationService.getExhibitor();
                this.$scope.visitorNumbers = '-';
                this.$scope.doScanner = this.doScanner.bind(this);
                this.applicationService.checkConsumerKey()
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        var exhibitor = {
                            name: '',
                            mail: '',
                            consumer_key: ''
                        };
                        _this.applicationService.getExhibitor().setExhibitor(exhibitor);
                    }
                    else {
                        var path = response.path;
                        _this.applicationService.getExhibitor().setName(response.exhibitor.name);
                        _this.applicationService.getExhibitor().setMail(response.exhibitor.mail);
                        _this.applicationService.getExhibitor().setConsumerKey(response.exhibitor.consumer_key);
                        _this.applicationService.getMasterConfig().URL_SET_SERIAL = path.serial;
                        _this.applicationService.getMasterConfig().URL_GET_VISITOR_COUNT = path.count;
                        _this.applicationService.getMasterConfig().URL_SET_CACHE = path.cache;
                    }
                    _this.getVisitorCount();
                });
            }
            /**
             * QRコードのスキャン
             */
            TrackingController.prototype.doScanner = function () {
                var _this = this;
                console.log(this.className + 'doScanner()');
                document.addEventListener("deviceready", function () {
                    _this.$cordovaBarcodeScanner.scan()
                        .then(function (barcodeData) {
                        console.log(barcodeData);
                        //console.log('path' + this.appliactionService.getMasterConfig().URL_SET_SERIAL);
                        // Success! Barcode data is here
                        if (barcodeData.text) {
                            _this.trackingService.postCode(barcodeData.text, _this.applicationService.getExhibitor(), _this.applicationService.getMasterConfig())
                                .success(function (response) {
                                console.log(response);
                                response = angular.fromJson(response);
                                if (response.error != '0') {
                                    _this.trackingService.saveCode(barcodeData.text, _this.applicationService.getMasterConfig());
                                    /* アラート作成 */
                                    var message = '送信できませんでした';
                                    var alertPopup = _this.$ionicPopup.alert({
                                        title: 'エラー',
                                        template: '<div style="text-align: center;">' + message + '</div>'
                                    });
                                    alertPopup.then(function (res) {
                                    });
                                }
                                else {
                                    /* キャッシュデータがあれば送信 */
                                    var cache = [];
                                    var tmp = _this.trackingService.showLocalData(_this.applicationService.getMasterConfig());
                                    //console.log('LocalCache');
                                    //console.log(tmp);
                                    if (tmp) {
                                        cache = tmp.cache;
                                    }
                                    if (cache.length != 0) {
                                        _this.trackingService.postCache(cache, _this.applicationService.getExhibitor(), _this.applicationService.getMasterConfig())
                                            .success(function (response) {
                                            console.log(response);
                                            response = angular.fromJson(response);
                                            if (response.error != '0') {
                                                var reCache = response.cache;
                                                _this.trackingService.saveCodes(reCache, _this.applicationService.getMasterConfig());
                                                var message = 'キャッシュ送信エラー';
                                                var alertPopup = _this.$ionicPopup.alert({
                                                    title: 'エラー',
                                                    template: '<div style="text-align: center;">' + message +
                                                        '</div>'
                                                });
                                                alertPopup.then(function (res) {
                                                });
                                            }
                                            else {
                                                _this.trackingService.saveCodes([], _this.applicationService.getMasterConfig());
                                                var alertPopup = _this.$ionicPopup.alert({
                                                    title: '成功',
                                                    template: '<div style="text-align: center;">全ての来場者データは送信されました</div>'
                                                });
                                                alertPopup.then(function (res) {
                                                });
                                                _this.getVisitorCount();
                                            }
                                        });
                                    }
                                }
                                _this.getVisitorCount();
                            })
                                .error(function () {
                                /* アラート作成 */
                                var message = barcodeData.text + 'を保存します';
                                var alertPopup = _this.$ionicPopup.alert({
                                    title: 'ネットワークエラー',
                                    template: '<div style="text-align: center;">' + message + '</div>'
                                });
                                alertPopup.then(function (res) {
                                });
                                _this.trackingService.saveCode(barcodeData.text, _this.applicationService.getMasterConfig());
                            });
                        }
                        else {
                        }
                    }, function (error) {
                        // An error occurred
                        console.log("Error-- POST serial");
                    });
                }, false);
            };
            /**
             * 来場者一覧取得
             */
            TrackingController.prototype.getVisitorCount = function () {
                var _this = this;
                console.log(this.className + 'getVisitorCount()');
                this.trackingService.getVisitorCount(this.applicationService.getExhibitor(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                    else {
                        var numbers = response.numbers;
                        _this.$scope.visitorNumbers = numbers.in_booth_all;
                    }
                })
                    .error(function () {
                    /* アラート作成 */
                    var message = '来場者情報が取得できませんでした';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                });
            };
            return TrackingController;
        })();
        Tracking.TrackingController = TrackingController;
        angular.module('Exhibitor.Tracking')
            .controller('TrackingController', ['$scope', '$ionicPopup', '$cordovaBarcodeScanner', 'TrackingService', 'ApplicationService',
            TrackingController]);
    })(Tracking = ST_Exhibitor.Tracking || (ST_Exhibitor.Tracking = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Tracking;
    (function (Tracking) {
        'use strict';
        var TrackingService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function TrackingService($http) {
                this.$http = $http;
                console.info('Create Class TrackingService');
            }
            /**
             * コード送信
             * @param code
             * @param exhibitor
             * @param config
             * @returns {any}
             */
            TrackingService.prototype.postCode = function (code, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + config.URL_SET_SERIAL,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        serial: code
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * 送信できなかった場合にシリアルコードをキャッシュ
             * @param code
             * @param config
             */
            TrackingService.prototype.saveCode = function (code, config) {
                var data = localStorage.getItem(config.KEY_SERIAL_CODE);
                if (data) {
                    data = angular.fromJson(data);
                    console.log('LocalCache');
                    console.log(data.cache);
                    var tmp = data.cache;
                    tmp.push(code);
                    var cache = {
                        cache: tmp
                    };
                    localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
                }
                else {
                    var tmp = [code];
                    var cache = {
                        cache: tmp
                    };
                    localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
                }
            };
            /**
             * シリアルコードを複数キャッシュ
             * @param codes
             * @param config
             */
            TrackingService.prototype.saveCodes = function (codes, config) {
                var cache = {
                    cache: codes
                };
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
            };
            /**
             * 来場者情報取得
             * @param exhibitor
             * @param config
             * @returns {any}
             */
            TrackingService.prototype.getVisitorCount = function (exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + config.URL_GET_VISITOR_COUNT,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey()
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * キャッシュデータの表示
             */
            TrackingService.prototype.showLocalData = function (config) {
                var data = localStorage.getItem(config.KEY_SERIAL_CODE);
                data = angular.fromJson(data);
                return data;
            };
            /**
             * キャッシュの送信
             * @param cache
             * @param exhibitor
             * @param config
             * @returns {any}
             */
            TrackingService.prototype.postCache = function (cache, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + config.URL_SET_CACHE,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        cache: cache
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            return TrackingService;
        })();
        Tracking.TrackingService = TrackingService;
        angular.module('Exhibitor.Tracking')
            .service('TrackingService', ['$http', TrackingService]);
    })(Tracking = ST_Exhibitor.Tracking || (ST_Exhibitor.Tracking = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
///<reference path="./module.ts"/>
///<reference path="./TrackingController.ts"/>
///<reference path="./TrackingService.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var VisitorList;
    (function (VisitorList) {
        'use strict';
        angular.module('Exhibitor.VisitorList', ['ngCordova', 'ionic'])
            .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
            }]);
    })(VisitorList = ST_Exhibitor.VisitorList || (ST_Exhibitor.VisitorList = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var VisitorList;
    (function (VisitorList) {
        'use strict';
        var VisitorListController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param $ionicPopup
             * @param visitorListService
             * @param applicationService
             */
            function VisitorListController($scope, $ionicPopup, visitorListService, applicationService) {
                this.$scope = $scope;
                this.$ionicPopup = $ionicPopup;
                this.visitorListService = visitorListService;
                this.applicationService = applicationService;
                this.className = 'VisitorListController #';
                console.info('Create Class VisitorListController');
                this.total = 0;
                this.$scope.visitorList = [];
                this.$scope.noMoreItemAvailable = false;
                this.$scope.getVisitorList = this.getVisitorList.bind(this);
                this.$scope.doRefresh = this.doRefresh.bind(this);
            }
            /**
             * 　Visitor一覧取得
             */
            VisitorListController.prototype.getVisitorList = function () {
                var _this = this;
                console.log(this.className + 'getVisitorList()');
                this.visitorListService.getVisitorList(this.applicationService.getExhibitor(), this.total, this.applicationService.getSettings(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    //console.log(response);
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                        _this.$scope.noMoreItemAvailable = true;
                    }
                    else {
                        response.visitor.forEach(function (v) {
                            var tmp = {
                                visitor_id: parseInt(v.visitor_id, 10),
                                name: v.name,
                                time: v.time
                            };
                            _this.$scope.visitorList.push(tmp);
                        });
                        _this.total = parseInt(response.total, 10);
                        var count = _this.applicationService.getSettings().getPager().value;
                        if (response.visitor.length === 0 || response.visitor.length < count) {
                            _this.$scope.noMoreItemAvailable = true;
                        }
                    }
                })
                    .error(function () {
                    _this.$scope.noMoreItemAvailable = true;
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                })
                    .finally(function () {
                    _this.$scope.$broadcast('scroll.infiniteScrollComplete');
                });
            };
            /**
             *
             */
            VisitorListController.prototype.doRefresh = function () {
                var _this = this;
                console.log(this.className + 'doRefresh()');
                this.total = 0;
                this.$scope.visitorList = [];
                this.$scope.noMoreItemAvailable = false;
                this.visitorListService.getVisitorList(this.applicationService.getExhibitor(), this.total, this.applicationService.getSettings(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    //console.log(response);
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                    else {
                        response.visitor.forEach(function (v) {
                            var tmp = {
                                visitor_id: parseInt(v.visitor_id, 10),
                                name: v.name,
                                time: v.time
                            };
                            _this.$scope.visitorList.push(tmp);
                        });
                        _this.total = parseInt(response.tottal, 10);
                        _this.$scope.noMoreItemAvailable = true;
                    }
                })
                    .error(function () {
                    _this.$scope.noMoreItemAvailable = true;
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                })
                    .finally(function () {
                    _this.$scope.$broadcast('scroll.infiniteScrollComplete');
                });
            };
            return VisitorListController;
        })();
        VisitorList.VisitorListController = VisitorListController;
        angular.module('Exhibitor.VisitorList')
            .controller('VisitorListController', ['$scope', '$ionicPopup', 'VisitorListService', 'ApplicationService', VisitorListController]);
    })(VisitorList = ST_Exhibitor.VisitorList || (ST_Exhibitor.VisitorList = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var VisitorList;
    (function (VisitorList) {
        'use strict';
        var VisitorListService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function VisitorListService($http) {
                this.$http = $http;
                console.info('Create Class VisitorListService');
            }
            /**
             *
             * @param exhibitor
             * @param config
             * @returns {IHttpPromise<T>}
             */
            VisitorListService.prototype.getVisitorList = function (exhibitor, total, setting, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + VisitorListService.URL_GET_VISITOR_ALL,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        total: total,
                        count: setting.getPager().value
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            VisitorListService.URL_GET_VISITOR_ALL = 'get_visitor_all.php';
            return VisitorListService;
        })();
        VisitorList.VisitorListService = VisitorListService;
        angular.module('Exhibitor.VisitorList')
            .service('VisitorListService', ['$http', VisitorListService]);
    })(VisitorList = ST_Exhibitor.VisitorList || (ST_Exhibitor.VisitorList = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var VisitorList;
    (function (VisitorList) {
        'use strict';
        var VisitorInfoController = (function () {
            function VisitorInfoController($scope, $stateParams, $ionicPopup, visitorInfoService, applicationService) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.$ionicPopup = $ionicPopup;
                this.visitorInfoService = visitorInfoService;
                this.applicationService = applicationService;
                this.className = 'VisitorInfoController #';
                console.info('Create Class VisitorInfoController');
                this.$scope.visitor = null;
                this.getVisitorInfo();
                this.$scope.setFavorite = this.setFavorite.bind(this);
                this.$scope.postNote = this.postNote.bind(this);
            }
            /**
             * Visitor詳細取得
             */
            VisitorInfoController.prototype.getVisitorInfo = function () {
                var _this = this;
                console.log(this.className + 'getVisitorInfo()');
                this.visitorInfoService.getVisitorInfo(this.$stateParams['visitorId'], this.applicationService.getExhibitor(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                    else {
                        var visitor = response.visitor;
                        _this.$scope.visitor = {
                            visitor_id: visitor.visitor_id,
                            content: visitor.content,
                            note: visitor.note,
                            favorite: (visitor.favorite === '1')
                        };
                    }
                })
                    .error(function () {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                });
            };
            /**
             * お気に入り設定
             * @param visitor
             */
            VisitorInfoController.prototype.setFavorite = function () {
                var _this = this;
                console.log(this.className + 'setFovorite()');
                this.$scope.visitor.favorite = !this.$scope.visitor.favorite;
                this.visitorInfoService.setFavorite(this.$scope.visitor, this.applicationService.getExhibitor(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'ネットワークエラー';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                })
                    .error(function () {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                });
            };
            /**
             * メモ送信
             */
            VisitorInfoController.prototype.postNote = function () {
                var _this = this;
                console.log(this.className + 'postNote()');
                this.visitorInfoService.postNote(this.$scope.visitor, this.applicationService.getExhibitor(), this.applicationService.getMasterConfig())
                    .success(function (response) {
                    response = angular.fromJson(response);
                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = 'ネットワークエラー';
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    }
                })
                    .error(function () {
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = _this.$ionicPopup.alert({
                        title: 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                });
            };
            return VisitorInfoController;
        })();
        VisitorList.VisitorInfoController = VisitorInfoController;
        angular.module('Exhibitor.VisitorList')
            .controller('VisitorInfoController', ['$scope', '$stateParams', '$ionicPopup', 'VisitorInfoService', 'ApplicationService', VisitorInfoController]);
    })(VisitorList = ST_Exhibitor.VisitorList || (ST_Exhibitor.VisitorList = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var VisitorList;
    (function (VisitorList) {
        'use strict';
        var VisitorInfoService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function VisitorInfoService($http) {
                this.$http = $http;
                console.info('Create Class VisitorInfoService');
            }
            /**
             *
             * @param id
             * @param exhibitor
             * @param config
             * @returns {IHttpPromise<T>}
             */
            VisitorInfoService.prototype.getVisitorInfo = function (id, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + VisitorInfoService.URL_GET_VISITOR,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        visitor_id: id
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * お気に入り設定
             * @param visitor
             * @param exhibitor
             * @param config
             * @returns {IHttpPromise<T>}
             */
            VisitorInfoService.prototype.setFavorite = function (visitor, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + VisitorInfoService.URL_SET_FAVORITE,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        visitor_id: visitor.visitor_id,
                        favorite: visitor.favorite ? '1' : '0'
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * メモ送信
             * @param visitor
             * @param exhibitor
             * @param config
             * @returns {IHttpPromise<T>}
             */
            VisitorInfoService.prototype.postNote = function (visitor, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + VisitorInfoService.URL_SET_NOTE,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        visitor_id: visitor.visitor_id,
                        note: visitor.note
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            VisitorInfoService.URL_GET_VISITOR = 'get_visitor.php';
            VisitorInfoService.URL_SET_FAVORITE = 'set_favorite.php';
            VisitorInfoService.URL_SET_NOTE = 'set_note.php';
            return VisitorInfoService;
        })();
        VisitorList.VisitorInfoService = VisitorInfoService;
        angular.module('Exhibitor.VisitorList')
            .service('VisitorInfoService', ['$http', VisitorInfoService]);
    })(VisitorList = ST_Exhibitor.VisitorList || (ST_Exhibitor.VisitorList = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
///<reference path="./module.ts"/>
///<reference path="./VisitorListController.ts"/>
///<reference path="./VisitorListService.ts"/>
///<reference path="./VisitorInfoController.ts"/>
///<reference path="./VisitorInfoService.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        angular.module('Exhibitor.Settings', ['ngCordova', 'ionic'])
            .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';
            }]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var SettingsController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param $ionicPopup
             * @param settingsService
             * @param applicationService
             */
            function SettingsController($scope, $state, $ionicHistory, $ionicPopup, settingsService, applicationService) {
                this.$scope = $scope;
                this.$state = $state;
                this.$ionicHistory = $ionicHistory;
                this.$ionicPopup = $ionicPopup;
                this.settingsService = settingsService;
                this.applicationService = applicationService;
                this.className = 'SettingsController# ';
                console.info('Create Class SettingsController');
                var cache = [];
                var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
                //console.log('LocalCache');
                //console.log(tmp);
                if (tmp) {
                    cache = tmp.cache;
                }
                this.$scope.numberCache = cache.length;
                this.$scope.showLocalData = this.showLocalData.bind(this);
                this.$scope.logout = this.logout.bind(this);
            }
            /**
             *
             */
            SettingsController.prototype.showLocalData = function () {
                var _this = this;
                console.log(this.className + 'showLocalData()');
                var cache = [];
                var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
                console.log('LocalCache');
                console.log(tmp);
                if (tmp) {
                    cache = tmp.cache;
                }
                if (cache.length != 0) {
                    var confirmPopup = this.$ionicPopup.confirm({
                        title: 'キャッシュデータ送信',
                        template: '<div style="text-align: center;">キャッシュデータが存在します送信しますか？</div>'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            _this.settingsService.postCache(cache, _this.applicationService.getExhibitor(), _this.applicationService.getMasterConfig())
                                .success(function (response) {
                                console.log(response);
                                response = angular.fromJson(response);
                                if (response.error != '0') {
                                    var reCache = response.cache;
                                    _this.settingsService.saveCodes(reCache, _this.applicationService.getMasterConfig());
                                    var message = 'ネットワークエラー';
                                    var alertPopup = _this.$ionicPopup.alert({
                                        title: 'エラー',
                                        template: '<div style="text-align: center;">' + message + '</div>'
                                    });
                                    alertPopup.then(function (res) {
                                    });
                                }
                                else {
                                    _this.settingsService.saveCodes([], _this.applicationService.getMasterConfig());
                                    _this.$scope.numberCache = 0;
                                    var alertPopup = _this.$ionicPopup.alert({
                                        title: '成功',
                                        template: '<div style="text-align: center;">全ての来場者データは送信されました</div>'
                                    });
                                    alertPopup.then(function (res) {
                                    });
                                }
                            })
                                .error(function () {
                                var message = '再度お試しください';
                                var alertPopup = _this.$ionicPopup.alert({
                                    title: 'ネットワークエラー',
                                    template: '<div style="text-align: center;">' + message + '</div>'
                                });
                                alertPopup.then(function (res) {
                                });
                            });
                        }
                        else {
                            /* アラート作成 */
                            var alertPopup = _this.$ionicPopup.alert({
                                title: '注意',
                                template: '<div style="text-align: center;">ログアウトする前に必ず送信するようにしてください</div>'
                            });
                            alertPopup.then(function (res) {
                            });
                        }
                    });
                }
                else {
                    var alertPopup = this.$ionicPopup.alert({
                        title: 'OK',
                        template: '<div style="text-align: center;">全ての来場者データは送信されています</div>'
                    });
                    alertPopup.then(function (res) {
                    });
                }
            };
            /**
             * ログアウト
             */
            SettingsController.prototype.logout = function () {
                var _this = this;
                console.log(this.className + 'logout()');
                var cache = [];
                var tmp = this.settingsService.showLocalData(this.applicationService.getMasterConfig());
                //console.log('LocalCache');
                //console.log(tmp);
                if (tmp) {
                    cache = tmp.cache;
                }
                if (cache.length != 0) {
                    this.settingsService.postCache(cache, this.applicationService.getExhibitor(), this.applicationService.getMasterConfig())
                        .success(function (response) {
                        response = angular.fromJson(response);
                        if (response.error != '0') {
                            var confirmPopup = _this.$ionicPopup.show({
                                title: 'エラー',
                                subtitle: 'ネットワークエラー',
                                template: '<div style="text-align: center;">このままログアウトすると来場者情報が削除されてしまいますがよろしいですか？</div>',
                                scope: _this.$scope,
                                buttons: [
                                    {
                                        text: 'ログアウトする',
                                        type: 'button-assertive',
                                        onTap: function (e) {
                                            _this.settingsService.logout(_this.applicationService.getExhibitor(), _this.applicationService.getMasterConfig());
                                            /* ページ遷移 */
                                            _this.$ionicHistory.nextViewOptions({
                                                disableBack: true
                                            });
                                            _this.$state.transitionTo('login');
                                        }
                                    },
                                    {
                                        text: 'キャンセル',
                                        type: 'button-positive',
                                        onTap: function (e) {
                                            var alertPopup = _this.$ionicPopup.alert({
                                                title: 'ログアウトしませんでした',
                                                template: '<div style="text-align: center;">ネットワークが繋がる場所で再度お試しください</div>'
                                            });
                                            alertPopup.then(function (res) {
                                            });
                                        }
                                    }
                                ]
                            });
                        }
                        else {
                            //キャッシュデータ送信成功
                            _this.settingsService.logout(_this.applicationService.getExhibitor(), _this.applicationService.getMasterConfig());
                            /* ページ遷移 */
                            _this.$ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            _this.$state.transitionTo('login');
                        }
                    })
                        .error(function () {
                        var alertPopup = _this.$ionicPopup.alert({
                            title: 'ログアウトしませんでした',
                            template: '<div style="text-align: center;">ネットワークが繋がる場所で再度お試しください</div>'
                        });
                        alertPopup.then(function (res) {
                        });
                    });
                }
                else {
                    //キャッシュデータなし
                    this.settingsService.logout(this.applicationService.getExhibitor(), this.applicationService.getMasterConfig());
                    /* ページ遷移 */
                    this.$ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    this.$state.transitionTo('login');
                }
            };
            return SettingsController;
        })();
        Settings.SettingsController = SettingsController;
        angular.module('Exhibitor.Settings')
            .controller('SettingsController', ['$scope', '$state', '$ionicHistory', '$ionicPopup', 'SettingsService', 'ApplicationService',
            SettingsController]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var SettingsService = (function () {
            /**
             *
             * @param $http
             */
            function SettingsService($http) {
                this.$http = $http;
                console.info('Create Class SettingsService');
            }
            /**
             * キャッシュデータの表示
             */
            SettingsService.prototype.showLocalData = function (config) {
                var data = localStorage.getItem(config.KEY_SERIAL_CODE);
                data = angular.fromJson(data);
                return data;
            };
            /**
             * ログアウト
             * @param exhibitor
             */
            SettingsService.prototype.logout = function (exhibitor, config) {
                /* Exhibitor情報削除*/
                exhibitor.removeLocalStrage();
                /* URL情報削除 */
                config.removeURLData();
                /* キャッシュデータの削除 */
                localStorage.removeItem(config.KEY_SERIAL_CODE);
            };
            /**
             * コード送信
             * @param code
             * @param exhibitor
             * @param config
             * @returns {any}
             */
            SettingsService.prototype.postCode = function (code, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + config.URL_SET_SERIAL,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        serial_code: code
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * キャッシュの送信
             * @param cache
             * @param exhibitor
             * @param config
             * @returns {any}
             */
            SettingsService.prototype.postCache = function (cache, exhibitor, config) {
                var req = {
                    method: 'POST',
                    url: config.domain + config.URL_SET_CACHE,
                    data: $.param({
                        consumer_key: exhibitor.getConsumerKey(),
                        cache: cache
                    }),
                    timeout: config.timeout
                };
                return this.$http(req);
            };
            /**
             * 送信できなかった場合にシリアルコードをキャッシュ
             * @param code
             * @param config
             */
            SettingsService.prototype.saveCode = function (code, config) {
                var data = localStorage.getItem(config.KEY_SERIAL_CODE);
                if (data) {
                    data = angular.fromJson(data);
                    var tmp = data.cache;
                    tmp.push(code);
                    var cache = {
                        cache: tmp
                    };
                    localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
                }
                else {
                    var tmp = [code];
                    var cache = {
                        cache: tmp
                    };
                    localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
                }
            };
            /**
             * シリアルコードを複数キャッシュ
             * @param codes
             * @param config
             */
            SettingsService.prototype.saveCodes = function (codes, config) {
                var cache = {
                    cache: codes
                };
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
            };
            return SettingsService;
        })();
        Settings.SettingsService = SettingsService;
        angular.module('Exhibitor.Settings')
            .service('SettingsService', ['$http', SettingsService]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var PagerController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param pagerService
             * @param applicationService
             */
            function PagerController($scope, pagerService, applicationService) {
                this.$scope = $scope;
                this.pagerService = pagerService;
                this.applicationService = applicationService;
                this.className = 'PagerController# ';
                console.info('Create Class PagerController');
                this.$scope.settings = this.applicationService.getSettings().getSettings();
                this.getPager();
                this.$scope.doChangePager = this.doChangePager.bind(this);
            }
            /**
             *
             */
            PagerController.prototype.getPager = function () {
                console.log(this.className + 'getPager()');
                var pager = this.applicationService.getSettings().getPager();
                this.$scope.pagerList = this.applicationService.getMasterConfig().pager;
                this.$scope.settings.pager = this.$scope.pagerList[this.pagerService.checkPager(pager, this.$scope.pagerList)];
            };
            /**
             *
             */
            PagerController.prototype.doChangePager = function () {
                console.log(this.className + 'doChangePager()');
                this.pagerService.doChangePager(this.$scope.settings.pager, this.applicationService.getSettings());
            };
            return PagerController;
        })();
        Settings.PagerController = PagerController;
        angular.module('Exhibitor.Settings')
            .controller('PagerController', ['$scope', 'PagerService', 'ApplicationService', PagerController]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var PagerService = (function () {
            /**
             * コンストラクタ
             * @param $http
             */
            function PagerService($http) {
                this.$http = $http;
                console.info('Create Class PagerService');
            }
            /**
             *
             * @param pager
             * @param settings
             */
            PagerService.prototype.doChangePager = function (pager, settings) {
                settings.setPager(pager);
                settings.setLocalStorage();
            };
            /**
             *
             * @param pager
             * @param pagerArray
             * @returns {number}
             */
            PagerService.prototype.checkPager = function (pager, pagerArray) {
                var index = -1;
                pagerArray.forEach(function (p, i) {
                    if (p.value === pager.value) {
                        index = i;
                    }
                });
                return index;
            };
            return PagerService;
        })();
        Settings.PagerService = PagerService;
        angular.module('Exhibitor.Settings')
            .service('PagerService', ['$http', PagerService]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var EventInfoController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param applicationService
             */
            function EventInfoController($scope, applicationService) {
                this.$scope = $scope;
                this.applicationService = applicationService;
                this.className = 'EventInfoController #';
                this.$scope.event = this.applicationService.getEvent().getEvent();
            }
            return EventInfoController;
        })();
        Settings.EventInfoController = EventInfoController;
        angular.module('Exhibitor.Settings')
            .controller('EventInfoController', ['$scope', 'ApplicationService', EventInfoController]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/08/26.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var Settings;
    (function (Settings) {
        'use strict';
        var ExhibitorInfoController = (function () {
            /**
             * コンストラクタ
             * @param $scope
             * @param applicationService
             */
            function ExhibitorInfoController($scope, applicationService) {
                this.$scope = $scope;
                this.applicationService = applicationService;
                this.className = 'ExhibitorInfoController #';
                console.info('Create Class ExhibitorInfoController');
                this.$scope.exhibitor = this.applicationService.getExhibitor().getExhibitor();
                this.$scope.exhibitor.consumer_key = '';
            }
            return ExhibitorInfoController;
        })();
        Settings.ExhibitorInfoController = ExhibitorInfoController;
        angular.module('Exhibitor.Settings')
            .controller('ExhibitorInfoController', ['$scope', 'ApplicationService', ExhibitorInfoController]);
    })(Settings = ST_Exhibitor.Settings || (ST_Exhibitor.Settings = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/09/03.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var About;
    (function (About) {
        'use strict';
        var AboutController = (function () {
            function AboutController($scope, aboutService, applicationService) {
                this.$scope = $scope;
                this.aboutService = aboutService;
                this.applicationService = applicationService;
                this.className = 'AboutController #';
                console.info('Create Class AboutController');
                this.$scope.version = this.applicationService.getVersion();
                this.$scope.goPolicy = this.goPolicy.bind(this);
            }
            AboutController.prototype.goPolicy = function () {
                console.log(this.className + 'goPolicy()');
                this.aboutService.goPolicy();
            };
            return AboutController;
        })();
        About.AboutController = AboutController;
        angular.module('Exhibitor.Settings')
            .controller('AboutController', ['$scope', 'AboutService', 'ApplicationService', AboutController]);
    })(About = ST_Exhibitor.About || (ST_Exhibitor.About = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/09/04.
 */
var ST_Exhibitor;
(function (ST_Exhibitor) {
    var About;
    (function (About) {
        'use strict';
        var AboutService = (function () {
            function AboutService($cordovaInAppBrowser) {
                this.$cordovaInAppBrowser = $cordovaInAppBrowser;
                console.info('Create Class AboutService');
            }
            AboutService.prototype.goPolicy = function () {
                var url = 'http://smilevision.co.jp/help/ppolicy.html';
                var options = {
                    location: 'no',
                    clearcache: 'yes',
                    toolbar: 'yes'
                };
                return this.$cordovaInAppBrowser.open(url, '_system', options);
            };
            return AboutService;
        })();
        About.AboutService = AboutService;
        angular.module('Exhibitor.Settings')
            .service('AboutService', ['$cordovaInAppBrowser', AboutService]);
    })(About = ST_Exhibitor.About || (ST_Exhibitor.About = {}));
})(ST_Exhibitor || (ST_Exhibitor = {}));
/**
 * Created by Higuchi Kengo on 15/07/31.
 */
///<reference path="./module.ts"/>
///<reference path="./SettingsController.ts"/>
///<reference path="./SettingsService.ts"/>
///<reference path="./PagerController.ts"/>
///<reference path="./PagerService.ts"/>
///<reference path="./EventInfoController.ts"/>
///<reference path="./ExhibitorInfoController.ts"/>
///<reference path="./AboutController.ts"/>
///<reference path="./AboutService.ts"/>
false;
/**
 * Created by Higuchi Kengo on 15/07/30.
 */
///<reference path="../typings/jquery/jquery.d.ts"/>
///<reference path="../typings/angularjs/angular.d.ts"/>
///<reference path="../typings/angular-ui-router/angular-ui-router.d.ts"/>
///<reference path="../typings/cordova/cordova.d.ts"/>
///<reference path="../typings/cordova-ionic/cordova-ionic.d.ts"/>
///<reference path="../typings/smile/exhibitor.d.ts"/>
///<reference path="./Model/Exhibitor.ts"/>
///<reference path="./Model/MasterConfig.ts"/>
///<reference path="./Model/Event.ts"/>
///<reference path="./Model/Setting.ts"/>
///<reference path="./Directive/index.ts"/>
///<reference path="./application/index.ts"/>
///<reference path="./login/index.ts"/>
///<reference path="./tracking/index.ts"/>
///<reference path="./visitorlist/index.ts"/>
///<reference path="./settings/index.ts"/>
var ST_Exhibitor;
(function (ST_Exhibitor) {
    'use strict';
    angular.module('starter', ['ionic',
        'ngCordova',
        'ngTouch',
        'ui.router',
        'Exhibitor.Application',
        'Exhibitor.Login',
        'Exhibitor.Directive',
        'Exhibitor.Tracking',
        'Exhibitor.VisitorList',
        'Exhibitor.Settings'])
        .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                window.StatusBar.styleLightContent();
            }
        });
    })
        .run(['$rootScope', '$state', '$ionicHistory', 'ApplicationService',
        function ($rootScope, $state, $ionicHistory, applicationService) {
            $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
                if (toState.isLoginRequired) {
                    if (!applicationService.isLoggedIn()) {
                        console.warn('Not Login');
                        e.preventDefault();
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('login');
                    }
                }
            });
        }])
        .config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            // Ionic uses AngularUI Router which uses the concept of states
            // Learn more here: https://github.com/angular-ui/ui-router
            // Set up the various states which the app can be in.
            // Each state's controller can be found in controllers.js
            $stateProvider
                .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginController'
            })
                .state('tabs', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/tabs.html',
                controller: 'ApplicationController',
                isLoginRequired: true
            })
                .state('tabs.tracking', {
                cache: false,
                url: '/tracking',
                views: {
                    'tracking': {
                        templateUrl: 'templates/tracking.html',
                        controller: 'TrackingController'
                    }
                },
                isLoginRequired: true
            })
                .state('tabs.visitorlist', {
                cache: false,
                url: '/visitorlist',
                views: {
                    'tracking': {
                        templateUrl: 'templates/visitorlist.html',
                        controller: 'VisitorListController'
                    }
                },
                isLoginRequired: true
            })
                .state('tabs.visitorinfo', {
                cache: false,
                url: '/visitorinfo/:visitorId',
                views: {
                    'tracking': {
                        templateUrl: 'templates/visitorinfo.html',
                        controller: 'VisitorInfoController'
                    }
                },
                isLoginRequired: true
            })
                .state('tabs.settings', {
                cache: false,
                url: '/settings',
                views: {
                    'settings': {
                        templateUrl: 'templates/settings.html',
                        controller: 'SettingsController'
                    }
                },
                isLoginRequired: true
            })
                .state('tabs.pager', {
                cache: false,
                url: '/pager',
                views: {
                    'settings': {
                        templateUrl: 'templates/change_pager.html',
                        controller: 'PagerController'
                    }
                }
            })
                .state('tabs.eventinfo', {
                url: '/eventinfo',
                views: {
                    'settings': {
                        templateUrl: 'templates/eventinfo.html',
                        controller: 'EventInfoController'
                    }
                }
            })
                .state('tabs.exhibitorinfo', {
                url: '/exhibitorinfo',
                views: {
                    'settings': {
                        templateUrl: 'templates/exhibitorinfo.html',
                        controller: 'ExhibitorInfoController'
                    }
                }
            })
                .state('tabs.about', {
                url: '/about',
                views: {
                    'settings': {
                        templateUrl: 'templates/about.html',
                        controller: 'AboutController'
                    }
                }
            });
            // if none of the above states are matched, use this as the fallback
            // to fix inifinite Loop
            $urlRouterProvider.otherwise(function ($injector) {
                var $state = $injector.get('$state');
                $state.go('tabs.tracking');
            });
        }])
        .config(function ($ionicConfigProvider) {
        $ionicConfigProvider.views.swipeBackEnabled(false);
    });
})(ST_Exhibitor || (ST_Exhibitor = {}));
//# sourceMappingURL=app.js.map