/**
 * Created by Higuchi Kengo on 15/07/07.
 */

/**
 * 出展者情報のインターフェース
 */
interface IExhibitor {
    name : string;
    tel ?: string;
    mail ?: string;
    consumer_key ?: string;
}

interface ILoginData {
    id : string;
    password : string;
}

interface IEvent {
    name : string;
    place : string;
    type : string;
}

interface IVisitor {
    visitor_id : number;
    name : string;
    time : string;
}

interface IContent {
    header : string;
    value : string;
}

interface IVisitorInfo {
    visitor_id : number;
    content : IContent[];
    note : string;
    favorite : boolean;
}
interface IPager {
    text : string;
    value : number;
}

interface ISettings {
    pager : IPager;
}

interface ICache{
    cache : string[]
}