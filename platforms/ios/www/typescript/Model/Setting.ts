/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor {
    'use strict';

    export class Setting {
        private static KEY_SETTINGS = 'KEY_SETTINGS';

        private pager : IPager;

        /**
         * コンストラクタ
         */
        public constructor () {
            console.info('Create Class Setting');
            this.setSettings(this.getLocalStorage());
        }

        /* Setter and Getter */

        public setSettings (settings : ISettings) {
            this.setPager(settings.pager);
            this.setLocalStorage();
        }

        public setPager (pager : IPager) : void {
            this.pager = pager;
        }

        public getSettings () : ISettings {
            return {
                pager: this.getPager()
            }
        }

        public getPager () : IPager {
            return this.pager;
        }

        /**
         *
         * @returns {*}
         */
        private getLocalStorage () : ISettings {
            var data = localStorage.getItem(Setting.KEY_SETTINGS);

            if (data) {
                data = angular.fromJson(data);
            } else {
                /* 設定情報が保存されていない時 */
                var master = new MasterConfig();

                data = {
                    pager: master.pager[0]
                }
            }

            return data;
        }

        /**
         *
         */
        public setLocalStorage () : void {
            localStorage.setItem(Setting.KEY_SETTINGS, angular.toJson(this.getSettings()));
        }

        /**
         *
         */
        public removeStorage () : void {
            localStorage.removeItem(Setting.KEY_SETTINGS);
        }
    }
}