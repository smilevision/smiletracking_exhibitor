/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Tracking {
    'use strict';

    export class TrackingService {

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class TrackingService');
        }

        /**
         * コード送信
         * @param code
         * @param exhibitor
         * @param config
         * @returns {any}
         */
        public postCode (code : string, exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + config.URL_SET_SERIAL,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    serial      : code
                }),
                timeout: config.timeout
            }

            return this.$http(req)
        }

        /**
         * 送信できなかった場合にシリアルコードをキャッシュ
         * @param code
         * @param config
         */
        public saveCode (code : string, config : MasterConfig) : void {
            var data = localStorage.getItem(config.KEY_SERIAL_CODE)

            if (data) {
                data = angular.fromJson(data);
                console.log('LocalCache');
                console.log(data.cache)
                var tmp : string[] = data.cache;
                tmp.push(code);
                var cache : ICache = {
                    cache: tmp
                }
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
            } else {
                var tmp : string[] = [code];
                var cache : ICache = {
                    cache: tmp
                }
                localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache))
            }
        }

        /**
         * シリアルコードを複数キャッシュ
         * @param codes
         * @param config
         */
        public saveCodes (codes : string[], config : MasterConfig) : void {
            var cache : ICache = {
                cache: codes
            }
            localStorage.setItem(config.KEY_SERIAL_CODE, angular.toJson(cache));
        }

        /**
         * 来場者情報取得
         * @param exhibitor
         * @param config
         * @returns {any}
         */
        public getVisitorCount (exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + config.URL_GET_VISITOR_COUNT,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey()
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }

        /**
         * キャッシュデータの表示
         */
        public showLocalData (config : MasterConfig) : ICache {
            var data = localStorage.getItem(config.KEY_SERIAL_CODE);

            data = angular.fromJson(data);
            return data;
        }

        /**
         * キャッシュの送信
         * @param cache
         * @param exhibitor
         * @param config
         * @returns {any}
         */
        public postCache (cache : string[], exhibitor : Exhibitor, config : MasterConfig) : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : config.domain + config.URL_SET_CACHE,
                data   : $.param({
                    consumer_key: exhibitor.getConsumerKey(),
                    cache       : cache
                }),
                timeout: config.timeout
            }

            return this.$http(req);
        }

    }

    angular.module('Exhibitor.Tracking')
        .service('TrackingService',
        ['$http', TrackingService])
}