/**
 * Created by Higuchi Kengo on 2016/01/05.
 */

namespace ST_Exhibitor.Directive {

    'use strict';

    interface Scope extends ng.IScope {

    }

    export class TouchStartDirectives implements ng.IDirective {
        public link : (scope : Scope,
                       element : ng.IAugmentedJQuery,
                       attrs : any) => void;

        //public restrict : string = 'AE';

        /**
         * コンストラクタ
         */
        public constructor (private $parse : ng.IParseService, private $swipe : any) {

            this.link = (scope : Scope, element : ng.IAugmentedJQuery, attrs : any) => {
                var handler = this.$parse(attrs.myTouchstart);
                this.$swipe.bind(element, {
                    start: function () {
                        scope.$apply(function () {
                            handler(scope);
                        });
                    }
                });
            }
        }
    }

    angular.module('Exhibitor.Directive')
        .directive('myTouchstart', ['$parse', '$swipe', ($parse : ng.IParseService, $swipe : any) => {
            return new TouchStartDirectives($parse, $swipe);
        }])
}