/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.VisitorList {
    'use strict';

    interface Scope extends ng.IScope {
        visitorList : IVisitor[];
        noMoreItemAvailable : boolean;

        getVisitorList() : void;
        doRefresh() : void;
    }

    export class VisitorListController {
        private className = 'VisitorListController #';

        private total : number;

        /**
         * コンストラクタ
         * @param $scope
         * @param $ionicPopup
         * @param visitorListService
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private $ionicPopup : any,
                            private visitorListService : VisitorListService,
                            private applicationService : Application.ApplicationService) {
            console.info('Create Class VisitorListController');

            this.total = 0;
            this.$scope.visitorList = [];
            this.$scope.noMoreItemAvailable = false;
            this.$scope.getVisitorList = this.getVisitorList.bind(this);
            this.$scope.doRefresh = this.doRefresh.bind(this);
        }

        /**
         * 　Visitor一覧取得
         */
        private getVisitorList () : void {
            console.log(this.className + 'getVisitorList()');

            this.visitorListService.getVisitorList(
                this.applicationService.getExhibitor(),
                this.total,
                this.applicationService.getSettings(),
                this.applicationService.getMasterConfig())
                .success((response) => {
                    //console.log(response);

                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                        this.$scope.noMoreItemAvailable = true;
                    } else {
                        response.visitor.forEach((v : any) => {
                            var tmp : IVisitor = {
                                visitor_id: parseInt(v.visitor_id, 10),
                                name      : v.name,
                                time      : v.time
                            }

                            this.$scope.visitorList.push(tmp);
                        });

                        this.total = parseInt(response.total, 10);
                        var count = this.applicationService.getSettings().getPager().value;

                        if (response.visitor.length === 0 || response.visitor.length < count) {
                            this.$scope.noMoreItemAvailable = true;
                        }
                    }

                })
                .error(() => {
                    this.$scope.noMoreItemAvailable = true;
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })
                .finally(() => {
                    this.$scope.$broadcast('scroll.infiniteScrollComplete');
                })
        }

        /**
         *
         */

        private doRefresh () : void {
            console.log(this.className + 'doRefresh()');

            this.total = 0;
            this.$scope.visitorList = [];
            this.$scope.noMoreItemAvailable = false;

            this.visitorListService.getVisitorList(
                this.applicationService.getExhibitor(),
                this.total,
                this.applicationService.getSettings(),
                this.applicationService.getMasterConfig())
                .success((response) => {
                    //console.log(response);

                    response = angular.fromJson(response);

                    if (response.error != '0') {
                        /* アラート作成 */
                        var message = '来場者情報が取得できませんでした';
                        var alertPopup = this.$ionicPopup.alert({
                            title   : 'エラー',
                            template: '<div style="text-align: center;">' + message + '</div>'
                        });

                        alertPopup.then((res : boolean) => {

                        });
                    } else {
                        response.visitor.forEach((v : any) => {
                            var tmp : IVisitor = {
                                visitor_id: parseInt(v.visitor_id, 10),
                                name      : v.name,
                                time      : v.time
                            }

                            this.$scope.visitorList.push(tmp);
                        });

                        this.total = parseInt(response.tottal, 10);
                        this.$scope.noMoreItemAvailable = true;
                    }

                })
                .error(() => {
                    this.$scope.noMoreItemAvailable = true;
                    /* アラート作成 */
                    var message = 'ネットワークエラー';
                    var alertPopup = this.$ionicPopup.alert({
                        title   : 'エラー',
                        template: '<div style="text-align: center;">' + message + '</div>'
                    });

                    alertPopup.then((res : boolean) => {

                    });
                })
                .finally(() => {
                    this.$scope.$broadcast('scroll.infiniteScrollComplete');
                })
        }
    }

    angular.module('Exhibitor.VisitorList')
        .controller('VisitorListController',
        ['$scope', '$ionicPopup', 'VisitorListService', 'ApplicationService', VisitorListController])
}