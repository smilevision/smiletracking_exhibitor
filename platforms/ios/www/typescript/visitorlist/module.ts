/**
 * Created by Higuchi Kengo on 15/08/26.
 */

namespace ST_Exhibitor.VisitorList {
    'use strict';

    angular.module('Exhibitor.VisitorList', ['ngCordova', 'ionic'])
        .config(['$httpProvider', ($httpProvider : any) => {
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';

        }]);
}