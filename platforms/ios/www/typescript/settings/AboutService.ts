/**
 * Created by Higuchi Kengo on 15/09/04.
 */

namespace ST_Exhibitor.About {
    'use strict';

    export class AboutService {

        public constructor (private $cordovaInAppBrowser : any) {
            console.info('Create Class AboutService');
        }

        public goPolicy () : ng.IPromise<any> {
            var url = 'http://smilevision.co.jp/help/ppolicy.html'
            var options = {
                location  : 'no',
                clearcache: 'yes',
                toolbar   : 'yes'
            };

            return this.$cordovaInAppBrowser.open(url, '_system', options)
        }
    }

    angular.module('Exhibitor.Settings')
        .service('AboutService',
        ['$cordovaInAppBrowser', AboutService]);
}