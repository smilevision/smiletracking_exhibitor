/**
 * Created by Higuchi Kengo on 15/07/31.
 */

namespace ST_Exhibitor.Login {
    'use strict';

    angular.module('Exhibitor.Login', ['ngCordova', 'ionic'])
        .config(['$httpProvider', ($httpProvider : any) => {
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;application/json;charset=utf-8';

        }]);
}