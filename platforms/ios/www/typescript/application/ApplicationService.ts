/**
 * Created by Higuchi Kengo on 15/07/30.
 */

namespace ST_Exhibitor.Application {
    'use strict';

    export class ApplicationService {

        /* 出展者 */
        private _exhibitor : Exhibitor;
        /* イベント */
        private _event : Event;
        /* config */
        private _masterConfig : MasterConfig;
        /* 設定*/
        private _settings : Setting;
        /* バージョン */
        private _version : string;

        /**
         * コンストラクタ
         * @param $http
         */
        public constructor (private $http : ng.IHttpService) {
            console.info('Create Class ApplicationService');

            this._exhibitor = new Exhibitor();
            this._event = new Event();
            this._masterConfig = new MasterConfig();
            this._settings = new Setting();
            this._version = '1.1.0';

            if (this._exhibitor.getConsumerKey() != '') {
                this.checkConsumerKey()
                    .success((response) => {
                        response = angular.fromJson(response);
                        if (response.error != '0') {
                            var exhibitor : IExhibitor = {
                                name        : '',
                                mail        : '',
                                consumer_key: ''
                            }

                            this._exhibitor.setExhibitor(exhibitor);
                        } else {

                            var path = response.path;
                            var event = response.event;

                            this._exhibitor.setName(response.exhibitor.name);
                            this._exhibitor.setMail(response.exhibitor.mail);
                            this._exhibitor.setConsumerKey(response.exhibitor.consumer_key);

                            this._event.setEvent({
                                name : event.name,
                                place: event.place,
                                type : event.type
                            })
                            this._masterConfig.URL_SET_SERIAL = path.serial;
                            this._masterConfig.URL_GET_VISITOR_COUNT = path.count;
                            this._masterConfig.URL_SET_CACHE = path.cache;

                        }
                    })

                    .error(() => {
                        var exhibitor : IExhibitor = {
                            name        : '',
                            mail        : '',
                            consumer_key: ''
                        }
                        this._exhibitor.setExhibitor(exhibitor);
                    })
            }
        }

        /* Getter */
        public getExhibitor () : Exhibitor {
            return this._exhibitor;
        }

        public getEvent () : Event {
            return this._event;
        }

        public getMasterConfig () : MasterConfig {
            return this._masterConfig;
        }

        public getSettings () : Setting {
            return this._settings;
        }

        public getVersion () : string {
            return this._version;
        }

        /**
         * ログインしているかどうか
         * @returns {boolean}
         */
        public isLoggedIn () : boolean {
            return (this._exhibitor.getConsumerKey() != '')
        }

        /**
         * コンシューマーキーのチェック
         * @returns {any}
         */
        public checkConsumerKey () : ng.IHttpPromise<any> {
            var req = {
                method : 'POST',
                url    : this._masterConfig.domain + 'validate_auth.php',
                data   : $.param({
                    consumer_key: this._exhibitor.getConsumerKey()
                }),
                timeout: this._masterConfig.timeout
            }

            return this.$http(req);
        }

        /**
         * アプリケーションの初期化
         */
        public removeData () : void {
            this._exhibitor.removeLocalStrage();

            this._exhibitor.setExhibitor({
                name        : '',
                consumer_key: ''
            })
        }
    }

    angular.module('Exhibitor.Application')
        .service('ApplicationService',
        ['$http', ApplicationService])
}