/**
 * Created by Higuchi Kengo on 15/07/30.
 */

namespace ST_Exhibitor.Application {
    'use strict';

    interface Scope extends ng.IScope {

    }

    export class ApplicationController {
        private className = 'ApplicationController# ';

        /**
         * コンストラクタ
         * @param $scope
         * @param applicationService
         */
        public constructor (private $scope : Scope,
                            private applicationService : ApplicationService) {
            console.info('Create Class ApplicationController')
        }
    }

    angular.module('Exhibitor.Application')
        .controller('ApplicationController',
        ['$scope', 'ApplicationService', ApplicationController])
}